#ifndef BRAINBIT_BLACK_DEVICE_H
#define BRAINBIT_BLACK_DEVICE_H


#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __ANDROID__
#include <jni.h>
#endif
	
#include "lib_export.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

	typedef struct _BrainbitBlackDetector BrainbitBlackDetector;
	
	typedef struct _BrainbitBlackScanner BrainbitBlackScanner;

	typedef struct _SignalData
	{
		unsigned short SampleNumber;
		unsigned char Marker;
		double O1;
		double T3;
		double T4;
		double O2;
	} SignalData;

	typedef struct _ResistanceData
	{
		double O1;
		double T3;
		double T4;
		double O2;
	} ResistanceData;

	typedef enum _ConnectionState
	{
		InRange,
		OutOfRange
	} ConnectionState;
	
	typedef enum _DeviceStatus
	{
		Invalid,
		PowerDown,
		Idle,
		Signal,
		Resist
	} DeviceStatus;

	typedef enum _DeviceError
	{
		NoError,
		Len,
		NoCommand
	} DeviceError;

	typedef struct _BrainbitBlackInfo
	{
		char Name[256];
		char Address[256];
		uint64_t SerialNumber;
	} BrainbitBlackInfo;

	typedef struct _BrainbitBlackInfoArray
	{
		BrainbitBlackInfo *info_array;
		size_t info_count;
	} BrainbitBlackInfoArray;

	SDK_SHARED void free_BrainbitBlackInfoArray(BrainbitBlackInfoArray);
	
	typedef void* DetectorListListenerHandle;
	typedef void* BatteryChargeListenerHandle;
	typedef void* DeviceStatusListenerHandle;
	typedef void* ConnectionStateListenerHandle;
	typedef void* SignalDataListenerHandle;
	typedef void* ResistanceDataListenerHandle;

	SDK_SHARED void free_DetectorListListenerHandle(DetectorListListenerHandle);
	SDK_SHARED void free_BatteryChargeListenerHandle(BatteryChargeListenerHandle);
	SDK_SHARED void free_DeviceStatusListenerHandle(DeviceStatusListenerHandle);
	SDK_SHARED void free_ConnectionStateListenerHandle(ConnectionStateListenerHandle);
	SDK_SHARED void free_SignalDataListenerHandle(SignalDataListenerHandle);
	SDK_SHARED void free_ResistanceDataListenerHandle(ResistanceDataListenerHandle);

#ifdef __ANDROID__
	SDK_SHARED BrainbitBlackScanner* create_scanner(JNIEnv *env, jobject context);
	SDK_SHARED JNIEnv* get_jni_env();
	SDK_SHARED jobject get_app_context(JNIEnv* env);
#else
	SDK_SHARED BrainbitBlackScanner* create_scanner();
#endif
	SDK_SHARED void scanner_delete(BrainbitBlackScanner* scanner);
	SDK_SHARED BrainbitBlackDetector* scanner_createDetector(BrainbitBlackScanner* scanner, char* identifier_string);
	SDK_SHARED int scanner_get_detectorList(BrainbitBlackScanner* scanner, BrainbitBlackInfoArray *out_info_array);
	SDK_SHARED int scanner_subscribe_detectorListChanged(BrainbitBlackScanner* scanner, void(*)(BrainbitBlackScanner*, void* user_data), DetectorListListenerHandle *, void *user_data);

	SDK_SHARED void detector_delete(BrainbitBlackDetector* detector);
	SDK_SHARED int detector_get_firmwareVersion(BrainbitBlackDetector* device, int* version);
	SDK_SHARED int detector_get_identifier(BrainbitBlackDetector* device, char* identifier_string, size_t string_length);
	SDK_SHARED int detector_get_state(BrainbitBlackDetector* device, DeviceStatus* out_status, DeviceError* out_error);
	SDK_SHARED int detector_get_connectionState(BrainbitBlackDetector* device, ConnectionState* out_state);
	SDK_SHARED int detector_get_batteryCharge(BrainbitBlackDetector* device, unsigned char *out_charge);
	SDK_SHARED int detector_subscribe_batteryChargeChanged(BrainbitBlackDetector* device, void(*)(BrainbitBlackDetector*, unsigned char, void *), BatteryChargeListenerHandle *, void *user_data);
	SDK_SHARED int detector_subscribe_stateChanged(BrainbitBlackDetector* device, void(*)(BrainbitBlackDetector*, DeviceStatus, DeviceError, void *), DeviceStatusListenerHandle *, void *user_data);
	SDK_SHARED int detector_subscribe_connectionStateChanged(BrainbitBlackDetector* device, void(*)(BrainbitBlackDetector*, ConnectionState, void *), ConnectionStateListenerHandle *, void *user_data);
	SDK_SHARED int detector_subscribe_signalChannel(BrainbitBlackDetector* device, void(*)(BrainbitBlackDetector*, SignalData, void *), SignalDataListenerHandle *, void *user_data);
	SDK_SHARED int detector_subscribe_resistanceChannel(BrainbitBlackDetector* device, void(*)(BrainbitBlackDetector*, ResistanceData, void *), ResistanceDataListenerHandle *, void *user_data);

	SDK_SHARED int detector_powerDown(BrainbitBlackDetector* device);
	SDK_SHARED int detector_goIdle(BrainbitBlackDetector* device);
	SDK_SHARED int detector_signal(BrainbitBlackDetector* device);
	SDK_SHARED int detector_resist(BrainbitBlackDetector* device);
	SDK_SHARED int detector_ping(BrainbitBlackDetector* device, unsigned char marker);
	
#ifdef __cplusplus
}
#endif
	
#endif // BRAINBIT_BLACK_DEVICE_H
