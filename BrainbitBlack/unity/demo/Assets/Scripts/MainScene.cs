﻿using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using BrainbitBlackSdk;

#if UNITY_ANDROID
using UnityEngine.Android;
#endif
public class MainScene : MonoBehaviour
{
    public Button BtnClose;
    public Button BtnConnect;
    public Button BtnResist;
    public Button BtnSignal;
    public Button BtnPing;
    public Button BtnGoIdle;
    public Button BtnPowerDown;
    public Text TxtState;
    public Text TxtCharge;
    public Text TxtStatus;
    public Text TxtData;

    private BrainbitBlackEnumerator _enumerator;
    private DeviceInfo? _deviceInfo = null;
    private BrainbitBlackDetector _device;

    private bool _connecting;

    private ViewState _currentState;
    private ViewState _viewState;


    private void CloseApp()
    {
        Application.Quit();
    }

    private void StartConnect()
    {
        _currentState.Data = "Scanning...";
        _currentState.BtnConnectEnabled = false;
        BtnConnect.enabled = false; // force
#if !UNITY_ANDROID
        Task.Run(() =>
        {
#endif
            try
            {
                _connecting = false;
                _enumerator = new BrainbitBlackEnumerator();
                _enumerator.EventDetectorListChanged += EventDetectorListChanged;
            }
            catch (System.Exception e)
            {
                _enumerator = null;
                _currentState.Data = $"Error: [{e}]";
            }
#if !UNITY_ANDROID
        });
#endif
    }

    private void StartResist()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    device.Resist();
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void StartSignal()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    device.Signal();
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void SendPingMarker()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    device.Ping(123);
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void GoIdle()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    device.GoIdle();
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void PowerDown()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    device.PowerDown();
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void EventDetectorListChanged(BrainbitBlackEnumerator enumerator)
    {
        if (_deviceInfo != null)
            return;
        try
        {
            var devLst = enumerator.Devices;
            if (devLst.Count > 0)
                _deviceInfo = devLst[0];

        }
        catch (System.Exception e)
        {
            _currentState.Data = $"Error: [{e}]";
        }
    }
    private void CheckScanDevice()
    {
        var devInf = _deviceInfo;
        if (devInf != null && !_connecting && !_currentState.Connected)
        {
            _connecting = true;
            _currentState.Data = "Connecting...";
#if !UNITY_ANDROID
            Task.Run(() =>
            {
#endif
                try
                {
                    ClearDevice();
                    var device = _enumerator.CreateDetector(devInf?.Address);

                    _currentState.DeviceInfoCommon = $"[Name]: [{devInf?.Name}]\n" +
                                        $"[Serial Number]: [{devInf?.SerialNumber}]\n" +
                                        $"[Address]: [{devInf?.Address}]\n" +
                                        $"[Firmware Version]: [{device.FirmwareVersion}]";

                    device.EventBatteryChargeChanged += EventBatteryChargeChanged;
                    device.EventDetectorConnectionState += EventDetectorConnectionState;
                    device.EventDetectorResistanceChannel += EventDetectorResistanceChannel;
                    device.EventDetectorSignalChannel += EventDetectorSignalChannel;
                    device.EventDetectorStateChanged += EventDetectorStateChanged;

                    _currentState.Charge = device.BatteryCharge;
                    _currentState.Status = device.DeviceState.Status;
                    _currentState.Connected = device.ConnectionState == ConnectionState.InRange;

                    _currentState.BtnResistEnabled = _currentState.Status != DeviceStatus.Resist;
                    _currentState.BtnSignalEnabled = _currentState.Status != DeviceStatus.Signal;
                    _currentState.BtnPingEnabled = _currentState.Status == DeviceStatus.Signal;
                    _currentState.BtnGoIdleEnabled = _currentState.Status != DeviceStatus.Idle;
                    _currentState.BtnPowerDownEnabled = _currentState.Status != DeviceStatus.PowerDown;
                    if (_currentState.Status == DeviceStatus.PowerDown || _currentState.Status == DeviceStatus.Idle)
                        _currentState.Data = _currentState.DeviceInfoCommon;

                    _device = device;
                }
                catch (System.Exception e)
                {
                    _deviceInfo = null;
                    _currentState.Data = $"Error: [{e}]";
                    _currentState.Connected = false;
                    _currentState.Status = DeviceStatus.Invalid;
                    ResetCurrentState();
                }
                finally
                {
                    _connecting = false;
                    try
                    {
                        ClearEnumerator();
                    }
                    catch (System.Exception e)
                    {
                        _currentState.Data = $"Error: [{e}]";
                    }
                }
#if !UNITY_ANDROID
            });
#endif
        }
    }

    private void EventBatteryChargeChanged(BrainbitBlackDetector detector, int charge)
    {
        _currentState.Charge = charge;
    }
    private void ResetCurrentState()
    {
        _currentState.BtnConnectEnabled = true;
        _currentState.BtnResistEnabled = false;
        _currentState.BtnSignalEnabled = false;
        _currentState.BtnPingEnabled = false;
        _currentState.BtnGoIdleEnabled = false;
        _currentState.BtnPowerDownEnabled = false;
        _currentState.Charge = 0;
        _currentState.DeviceInfoCommon = string.Empty;
    }
    private void EventDetectorConnectionState(BrainbitBlackDetector detector, ConnectionState connectionState)
    {
        bool connected = connectionState == ConnectionState.InRange;
        if (connected != _currentState.Connected)
        {
            _currentState.Connected = connected;
            if (!connected)
            {
                ResetCurrentState();
            }
        }
    }
    private void EventDetectorResistanceChannel(BrainbitBlackDetector detector, ResistanceData resistanceData)
    {
        _currentState.Data = string.Format("[O1]:[{0:0.00} kOm]\n[T3]:[{1:0.00} kOm]\n[T4]:[{2:0.00} kOm]\n[O2]:[{3:0.00} kOm]\n",
            double.IsInfinity(resistanceData.O1) ? resistanceData.O1 : resistanceData.O1 / 1000.0,
            double.IsInfinity(resistanceData.T3) ? resistanceData.T3 : resistanceData.T3 / 1000.0,
            double.IsInfinity(resistanceData.T4) ? resistanceData.T4 : resistanceData.T4 / 1000.0,
            double.IsInfinity(resistanceData.O2) ? resistanceData.O2 : resistanceData.O2 / 1000.0
            );
    }
    private void EventDetectorSignalChannel(BrainbitBlackDetector detector, SignalData signalData)
    {
        _currentState.Data = string.Format("[Pack number]:[{0:0}]\n[Marker][{1:0}]\n[O1]:[{2:+00000.000;-00000.000} uV]\n[T3]:[{3:+00000.000;-00000.000} uV]\n[T4]:[{4:+00000.000;-00000.000} uV]\n[O2]:[{5:+00000.000;-00000.000} uV]\n",
            signalData.SampleNumber,
            signalData.Marker,
            signalData.O1 * 1e6,
            signalData.T3 * 1e6,
            signalData.T4 * 1e6,
            signalData.O2 * 1e6
            );
    }
    private void EventDetectorStateChanged(BrainbitBlackDetector detector, DeviceState state)
    {
        if (_currentState.Status != state.Status)
        {
            _currentState.Status = state.Status;
            _currentState.BtnResistEnabled = _currentState.Status != DeviceStatus.Resist;
            _currentState.BtnSignalEnabled = _currentState.Status != DeviceStatus.Signal;
            _currentState.BtnPingEnabled = _currentState.Status == DeviceStatus.Signal;
            _currentState.BtnGoIdleEnabled = _currentState.Status != DeviceStatus.Idle;
            _currentState.BtnPowerDownEnabled = _currentState.Status != DeviceStatus.PowerDown;
            if (_currentState.Status == DeviceStatus.PowerDown || _currentState.Status == DeviceStatus.Idle)
                _currentState.Data = _currentState.DeviceInfoCommon;
        }
    }

    private void InitView()
    {
        if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            BtnClose.gameObject.SetActive(false);
        }
        BtnClose.onClick.AddListener(CloseApp);
        BtnConnect.onClick.AddListener(StartConnect);
        BtnResist.onClick.AddListener(StartResist);
        BtnSignal.onClick.AddListener(StartSignal);
        BtnPing.onClick.AddListener(SendPingMarker);
        BtnGoIdle.onClick.AddListener(GoIdle);
        BtnPowerDown.onClick.AddListener(PowerDown);

        ResetCurrentState();

        _viewState = _currentState;

        BtnConnect.enabled = true;
        BtnResist.enabled = false;
        BtnSignal.enabled = false;
        BtnPing.enabled = false;
        BtnGoIdle.enabled = false;
        BtnPowerDown.enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        InitView();
#if UNITY_ANDROID
        Permission.RequestUserPermission("android.permission.BLUETOOTH");
        Permission.RequestUserPermission("android.permission.BLUETOOTH_ADMIN");
        Permission.RequestUserPermission("android.permission.ACCESS_FINE_LOCATION");
        Permission.RequestUserPermission("android.permission.ACCESS_COARSE_LOCATION");
#endif
    }

    private void UpdateStatePanel()
    {
        if (_currentState.Connected != _viewState.Connected)
        {
            _viewState.Connected = _currentState.Connected;
            TxtState.text = _viewState.Connected ? "Connected" : "Disconnected";
        }
        if (_currentState.Charge != _viewState.Charge)
        {
            _viewState.Charge = _currentState.Charge;
            TxtCharge.text = _viewState.Charge > 0 ? $"{_viewState.Charge}%" : "-";
        }
        if (_currentState.DeviceInfoCommon != _viewState.DeviceInfoCommon)
            _viewState.DeviceInfoCommon = _currentState.DeviceInfoCommon;
        if (_currentState.Status != _viewState.Status)
        {
            _viewState.Status = _currentState.Status;
            TxtStatus.text = _viewState.Status != DeviceStatus.Invalid ? _viewState.Status.ToString() : "-";
        }
    }

    private void UpdateViewState()
    {
        UpdateStatePanel();

        if (_currentState.BtnConnectEnabled != _viewState.BtnConnectEnabled)
        {
            _viewState.BtnConnectEnabled = _currentState.BtnConnectEnabled;
            BtnConnect.enabled = _viewState.BtnConnectEnabled;
        }
        if (_currentState.BtnResistEnabled != _viewState.BtnResistEnabled)
        {
            _viewState.BtnResistEnabled = _currentState.BtnResistEnabled;
            BtnResist.enabled = _viewState.BtnResistEnabled;
        }
        if (_currentState.BtnSignalEnabled != _viewState.BtnSignalEnabled)
        {
            _viewState.BtnSignalEnabled = _currentState.BtnSignalEnabled;
            BtnSignal.enabled = _viewState.BtnSignalEnabled;
        }
        if (_currentState.BtnPingEnabled != _viewState.BtnPingEnabled)
        {
            _viewState.BtnPingEnabled = _currentState.BtnPingEnabled;
            BtnPing.enabled = _viewState.BtnPingEnabled;
        }
        if (_currentState.BtnGoIdleEnabled != _viewState.BtnGoIdleEnabled)
        {
            _viewState.BtnGoIdleEnabled = _currentState.BtnGoIdleEnabled;
            BtnGoIdle.enabled = _viewState.BtnGoIdleEnabled;
        }
        if (_currentState.BtnPowerDownEnabled != _viewState.BtnPowerDownEnabled)
        {
            _viewState.BtnPowerDownEnabled = _currentState.BtnPowerDownEnabled;
            BtnPowerDown.enabled = _viewState.BtnPowerDownEnabled;
        }
        if (_currentState.Data != _viewState.Data)
        {
            _viewState.Data = _currentState.Data;
            TxtData.text = _viewState.Data;
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckScanDevice();
        UpdateViewState();
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            CloseApp();
        }
    }

    private void ClearDevice()
    {
        var device = _device;
        _device = null;
        if (device != null)
        {
            ResetCurrentState();

            device.EventBatteryChargeChanged -= EventBatteryChargeChanged;
            device.EventDetectorConnectionState -= EventDetectorConnectionState;
            device.EventDetectorResistanceChannel -= EventDetectorResistanceChannel;
            device.EventDetectorSignalChannel -= EventDetectorSignalChannel;
            device.EventDetectorStateChanged -= EventDetectorStateChanged;
            device.Dispose();
        }
    }
    private void ClearEnumerator()
    {
        var enumerator = _enumerator;
        _enumerator = null;
        if (enumerator != null)
        {
            enumerator.EventDetectorListChanged -= EventDetectorListChanged;
            enumerator.Dispose();
        }
    }

    private void OnApplicationQuit()
    {
        try
        {
            var device = _device;
            if (device != null && device.ConnectionState == ConnectionState.InRange)
                device.GoIdle();
            ClearDevice();
            ClearEnumerator();
        }
        catch (System.Exception)
        {
        }
        finally
        {
#if !UNITY_EDITOR
            // Kill all native threads
            System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
        }
    }

    /// <summary>
    /// State of visual components.<br>
    /// Not thread-safe
    /// </summary>
    private struct ViewState
    {
        public bool Connected;
        public int Charge;
        public DeviceStatus Status;
        public string Data;

        public bool BtnConnectEnabled;
        public bool BtnResistEnabled;
        public bool BtnSignalEnabled;
        public bool BtnPingEnabled;
        public bool BtnGoIdleEnabled;
        public bool BtnPowerDownEnabled;

        public string DeviceInfoCommon;
    }
}
