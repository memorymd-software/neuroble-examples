package com.neurosdk.p300.demo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.neuromd.p300sdk.ConnectionState;
import com.neuromd.p300sdk.DeviceState;
import com.neuromd.p300sdk.DeviceStatus;
import com.neuromd.p300sdk.P300Detector;
import com.neuromd.p300sdk.P300Enumerator;
import com.neuromd.p300sdk.P300Info;
import com.neuromd.p300sdk.ResistanceData;
import com.neuromd.p300sdk.SignalData;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class MainActivity extends AppCompatActivity {
    private Button btnConnect;
    private Button btnResist;
    private Button btnSignal;
    private Button btnPing;
    private Button btnGoIdle;
    private Button btnPowerDown;

    private TextView txtDevConnectionState;
    private TextView txtDevBatteryPower;
    private TextView txtDevState;
    private TextView txtDevInfo;
    private TextView txtRawData;

    private SensorHelper sensorHelper;
    private P300Detector p300Device;
    private final AtomicBoolean thConnection = new AtomicBoolean();
    private final AtomicBoolean thRawDataUpd = new AtomicBoolean();

    private final AtomicReference<SignalData> signalDataLast = new AtomicReference<>();
    private final AtomicReference<ResistanceData> resistanceDataLast = new AtomicReference<>();
    private final AtomicReference<DeviceStatus> deviceStatusLast = new AtomicReference<>(DeviceStatus.PowerDown);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorHelper = new SensorHelper(this);

        btnConnect = findViewById(R.id.btn_connect);
        btnResist = findViewById(R.id.btn_resist);
        btnSignal = findViewById(R.id.btn_signal);
        btnPing = findViewById(R.id.btn_ping);
        btnGoIdle = findViewById(R.id.btn_go_idle);
        btnPowerDown = findViewById(R.id.btn_power_down);

        txtDevConnectionState = findViewById(R.id.txt_dev_connection_state);
        txtDevBatteryPower = findViewById(R.id.txt_dev_battery_power);
        txtDevState = findViewById(R.id.txt_dev_state);
        txtDevInfo = findViewById(R.id.txt_dev_info);
        txtRawData = findViewById(R.id.txt_raw_data);

        setButtonState(false);

        btnConnect.setOnClickListener(v -> {
            connectionStateChanged(false);
            btnConnect.setEnabled(false);
            sensorHelper.enabledSensor(new SensorHelper.ISensorEvent() {
                @Override
                public void ready() {
                    connectToDevice();
                }

                @Override
                public void cancel(String message, Exception error) {
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                    btnConnect.setEnabled(true);
                }
            });
        });

        btnResist.setOnClickListener(v -> p300Device.resist());

        btnSignal.setOnClickListener(v -> p300Device.signal());

        btnPing.setOnClickListener(v -> p300Device.ping((byte) 108));

        btnGoIdle.setOnClickListener(v -> p300Device.goIdle());

        btnPowerDown.setOnClickListener(v -> p300Device.powerDown());
    }

    @Override
    protected void onDestroy() {
        thConnection.set(false);
        thRawDataUpd.set(false);
        super.onDestroy();
    }

    private void setButtonState(boolean enabled) {
        btnResist.setEnabled(enabled);
        btnSignal.setEnabled(enabled);
        btnPing.setEnabled(enabled);
        btnGoIdle.setEnabled(enabled);
        btnPowerDown.setEnabled(enabled);
    }

    private void freeDevice() {
        P300Detector dev = p300Device;
        p300Device = null;
        if (dev != null) {
            try {
                dev.close();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void connectToDevice() {
        if (!thConnection.getAndSet(true)) {
            Thread th = new Thread(() -> {
                P300Enumerator enumerator = new P300Enumerator(getApplicationContext(), new P300Enumerator.EnumeratorCallback() {
                    @Override
                    public void onDeviceListChanged() {

                    }
                });
                List<P300Info> devices;
                do {
                    devices = enumerator.devices();
                } while (devices.isEmpty() && !Thread.currentThread().isInterrupted() && thConnection.get());
                if (!thConnection.get() || devices.isEmpty())
                    return;

                try {
                    final P300Info devInfo = devices.get(0);
                    while (p300Device == null || p300Device.getConnectionState() != ConnectionState.InRange) {
                        freeDevice();
                        p300Device = enumerator.createDetector(devInfo.address(), new P300Detector.DeviceCallback() {
                            @Override
                            public void onBatteryChargeChanged(final int val) {
                                runOnUiThread(() -> updateBattery(val));
                            }

                            @Override
                            public void onConnectionStateChanged(final ConnectionState connectionState) {
                                runOnUiThread(() -> connectionStateChanged(connectionState == ConnectionState.InRange));
                            }

                            @Override
                            public void onDeviceStateChanged(final DeviceState deviceState) {
                                deviceStatusLast.set(deviceState.getStatus());
                                runOnUiThread(() -> updateDevState());
                            }

                            @Override
                            public void onSignalReceived(SignalData signalData) {
                                signalDataLast.set(signalData);
                            }

                            @Override
                            public void onResistanceReceived(ResistanceData resistanceData) {
                                resistanceDataLast.set(resistanceData);
                            }
                        });
                    }
                    enumerator.close();

                    runOnUiThread(() -> {
                        boolean state = p300Device.getConnectionState() == ConnectionState.InRange;
                        setButtonState(state);
                        connectionStateChanged(state);
                        if (state) {
                            updateBattery(p300Device.getBatteryCharge());

                            final StringBuilder sb = new StringBuilder();
                            sb.append("Name: [").append(devInfo.name()).append("] SN: [").append(devInfo.serialNumber()).append("]\n");
                            sb.append("Identifier: [").append(p300Device.getIdentifier()).append("] Version: [").append(p300Device.getFirmwareVersion()).append("]");
                            txtDevInfo.setText(sb.toString());

                            deviceStatusLast.set(p300Device.getDeviceState().getStatus());
                            updateDevState();

                            startUpdateRawData();
                        }
                    });
//                } catch (InterruptedException ignored) {
                } catch (Exception e) {
                    runOnUiThread(() -> {
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        btnConnect.setEnabled(true);
                    });
                } finally {
                    thConnection.set(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }

    private void connectionStateChanged(final boolean state) {
        btnConnect.setEnabled(!state);
        setButtonState(state);
        if (state) {
            txtDevConnectionState.setText(R.string.dev_state_connected);
        } else {
            txtDevConnectionState.setText(R.string.dev_state_disconnected);
            txtDevBatteryPower.setText(R.string.dev_power_empty);
            txtDevState.setText(R.string.dev_power_empty);
            txtDevInfo.setText(null);

            if (p300Device != null) {
                Thread th = new Thread(() -> {
                    try {
                        freeDevice();
                    } catch (Exception ignored) {
                    }
                });
                th.setDaemon(true);
                th.start();
            }
        }
    }

    private void updateBattery(final int val) {
        txtDevBatteryPower.setText(getString(R.string.dev_power_prc, val));
    }

    private void updateDevState() {
        DeviceStatus status = deviceStatusLast.get();
        if (status != null)
            txtDevState.setText(String.valueOf(status));
        else
            txtDevState.setText(null);
    }

    @SuppressWarnings("BusyWait")
    private void startUpdateRawData() {
        if (!thRawDataUpd.getAndSet(true)) {
            Thread th = new Thread(() -> {
                try {
                    while (thRawDataUpd.get() && !Thread.currentThread().isInterrupted()) {
                        if (p300Device != null) {
                            final StringBuilder sb = new StringBuilder();
                            DeviceStatus status = deviceStatusLast.get();
                            if (status == DeviceStatus.Resist) {
                                ResistanceData data = resistanceDataLast.get();
                                if (data != null) {
                                    if (data.getO1() >= Double.POSITIVE_INFINITY)
                                        sb.append("O1: [∞]\n");
                                    else
                                        sb.append("O1: [").append(Math.round(data.getO1() / 1000.0)).append(" kΩ]\n");
                                    if (data.getO2() >= Double.POSITIVE_INFINITY)
                                        sb.append("O2: [∞]\n");
                                    else
                                        sb.append("O2: [").append(Math.round(data.getO2() / 1000.0)).append(" kΩ]\n");
                                    if (data.getC3() >= Double.POSITIVE_INFINITY)
                                        sb.append("C3: [∞]\n");
                                    else
                                        sb.append("C3: [").append(Math.round(data.getC3() / 1000.0)).append(" kΩ]\n");
                                    if (data.getCZ() >= Double.POSITIVE_INFINITY)
                                        sb.append("CZ: [∞]\n");
                                    else
                                        sb.append("CZ: [").append(Math.round(data.getCZ() / 1000.0)).append(" kΩ]\n");
                                    if (data.getC4() >= Double.POSITIVE_INFINITY)
                                        sb.append("C4: [∞]\n");
                                    else
                                        sb.append("C4: [").append(Math.round(data.getC4() / 1000.0)).append(" kΩ]\n");
                                    if (data.getFZ() >= Double.POSITIVE_INFINITY)
                                        sb.append("FZ: [∞]\n");
                                    else
                                        sb.append("FZ: [").append(Math.round(data.getFZ() / 1000.0)).append(" kΩ]\n");
                                    if (data.getPZ() >= Double.POSITIVE_INFINITY)
                                        sb.append("PZ: [∞]\n");
                                    else
                                        sb.append("PZ: [").append(Math.round(data.getPZ() / 1000.0)).append(" kΩ]");
                                }
                            } else if (status == DeviceStatus.Signal) {
                                SignalData data = signalDataLast.get();
                                if (data != null) {
                                    sb.append("Num: [").append(data.getSampleNumber()).append("]\n");
                                    sb.append("Marker: [").append(data.getMarker()).append("]\n");
                                    sb.append("O1: [").append(data.getO1()).append("]\n");
                                    sb.append("O2: [").append(data.getO2()).append("]\n");
                                    sb.append("C3: [").append(data.getC3()).append("]\n");
                                    sb.append("CZ: [").append(data.getCZ()).append("]\n");
                                    sb.append("C4: [").append(data.getC4()).append("]\n");
                                    sb.append("FZ: [").append(data.getFZ()).append("]\n");
                                    sb.append("PZ: [").append(data.getPZ()).append("]");
                                }
                            }
                            runOnUiThread(() -> txtRawData.setText(sb.toString()));
                        }
                        Thread.sleep(250);
                    }
                } catch (Exception ignored) {
                } finally {
                    thRawDataUpd.set(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }
}