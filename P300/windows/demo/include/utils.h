#ifndef MAIN_UTILS_H
#define MAIN_UTILS_H

#include <stdint.h>
#include "p300.h"

void demo_sleep_ms(uint32_t);

const char* device_status_to_string(const DeviceStatus);
const char* device_error_to_string(const DeviceError);
#endif // MAIN_UTILS_H