#include "p300.h"
#include "sdk_error.h"
#include "utils.h"

#include "stdio.h"
#include <float.h>

#define SIGNAL_SAMPLES_COUNT 20
#define RESIST_SAMPLES_COUNT 3

void display_sdk_error(const char* msg) {
	char errorMsg[1024];
	sdk_last_error_msg(errorMsg, 1024);
	printf("%s. Error desc: %s\n", msg, errorMsg);
}

void display_info(P300Detector* device, P300Info* info) {
	printf("[Device]:[%s]\n[Serial number]:[%llu]\n[Address]:[%s]\n", info->Name, info->SerialNumber, info->Address);

	int version;
	if (detector_get_firmwareVersion(device, &version) == SDK_NO_ERROR) {
		printf("[Version]:[%d]\n", version);
	}
	else {
		display_sdk_error("Cannot get device version");
	}
}

void display_battery_power(P300Detector* device) {
	unsigned char batPower = 0;
	if (detector_get_batteryCharge(device, &batPower) == SDK_NO_ERROR) {
		printf("[Battery power]:[%d%%]\n", batPower);
	}
	else
	{
		display_sdk_error("Cannot get battery power");
	}
}

void display_state(P300Detector* device) {
	DeviceStatus devStatus;
	DeviceError devError;
	if (detector_get_state(device, &devStatus, &devError) == SDK_NO_ERROR) {
		printf("[Device status]:[%s]\n[Device error]:[%s]\n", device_status_to_string(devStatus), device_error_to_string(devError));
	}
	else {
		display_sdk_error("Cannot get state");
	}
}

//-----------------------------------------------------------------------------------
//------------------------------ Signal ---------------------------------------------
//-----------------------------------------------------------------------------------
typedef struct _SignalUserData {
	SignalData data[SIGNAL_SAMPLES_COUNT];
	size_t idx;
	// data buffer is full
	bool dataFilled;
} SignalUserData;

void signal_data_process(P300Detector* device, SignalData data, void* user_data) {
	SignalUserData* signalUserData = (SignalUserData*)user_data;
	if (!signalUserData->dataFilled) {
		signalUserData->data[signalUserData->idx++] = data;
		if (signalUserData->idx == SIGNAL_SAMPLES_COUNT)
			signalUserData->dataFilled = true;
	}
}

void display_signal(P300Detector* device) {
	printf("\n----------- Signal -----------\n");

	SignalDataListenerHandle handler;
	SignalUserData signalUserData;
	signalUserData.idx = 0;
	signalUserData.dataFilled = false;
	if (detector_subscribe_signalChannel(device, signal_data_process, &handler, &signalUserData) != SDK_NO_ERROR) {
		display_sdk_error("Cannot subscribe signal channel");
		return;
	}
	// start recive signal
	if (detector_signal(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot start signal channel");
		return;
	}

	int attempt = 10;
	while (attempt > 0) {
		demo_sleep_ms(10);
		if (signalUserData.dataFilled) {
			printf("[N]\t[M]\t    [O1]\t     [C3]\t     [CZ]\t     [PZ]\t     [O2]\t     [C4]\t     [FZ]\n");
			for (int i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
				SignalData data = signalUserData.data[i];
				printf("%u\t%u\t%.2fuV\t%.2fuV\t%.2fuV\t%.2fuV\t%.2fuV\t%.2fuV\t%.2fuV\n",
					data.SampleNumber,
					data.Marker,
					data.O1 * 1e6,
					data.C3 * 1e6,
					data.CZ * 1e6,
					data.PZ * 1e6,
					data.O2 * 1e6,
					data.C4 * 1e6,
					data.FZ * 1e6
				);
			}
			signalUserData.idx = 0;
			signalUserData.dataFilled = false;
			--attempt;

			// send marker ping
			if (detector_ping(device, 123) != SDK_NO_ERROR) {
				display_sdk_error("Cannot ping");
				break;
			}
		}
	}
	// stop recive signal
	if (detector_powerDown(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot stop signal channel. Failed PowerDown mode work");
	}
	free_SignalDataListenerHandle(handler);
}
//-----------------------------------------------------------------------------------
//------------------------------ Resist ---------------------------------------------
//-----------------------------------------------------------------------------------
typedef struct _ResistanceUserData {
	ResistanceData data[RESIST_SAMPLES_COUNT];
	size_t idx;
	// data buffer is full
	bool dataFilled;
} ResistanceUserData;

void resist_data_process(P300Detector* device, ResistanceData data, void* user_data) {
	ResistanceUserData* signalUserData = (ResistanceUserData*)user_data;
	if (!signalUserData->dataFilled) {
		signalUserData->data[signalUserData->idx++] = data;
		if (signalUserData->idx == RESIST_SAMPLES_COUNT)
			signalUserData->dataFilled = true;
	}
}

void display_resist_value(double val) {
	if (val >= DBL_MAX)
		printf("[-----] kOm\t");
	else
		printf("[%.2f] kOm\t", val / 1000);
}

void display_resist(P300Detector* device) {
	printf("\n----------- Resistance -----------\n");

	ResistanceDataListenerHandle handler;
	ResistanceUserData resistanceUserData;
	resistanceUserData.idx = 0;
	resistanceUserData.dataFilled = false;
	if (detector_subscribe_resistanceChannel(device, resist_data_process, &handler, &resistanceUserData) != SDK_NO_ERROR) {
		display_sdk_error("Cannot subscribe signal channel");
		return;
	}
	// start recive resistance
	if (detector_resist(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot start resistance channel");
		return;
	}

	int attempt = 20;
	while (attempt > 0) {
		demo_sleep_ms(10);
		if (resistanceUserData.dataFilled) {
			printf("    [O1]\t     [C3]\t     [CZ]\t     [PZ]\t     [O2]\t     [C4]\t     [FZ]\n");
			for (int i = 0; i < RESIST_SAMPLES_COUNT; ++i) {
				ResistanceData data = resistanceUserData.data[i];
				display_resist_value(data.O1);
				display_resist_value(data.C3);
				display_resist_value(data.CZ);
				display_resist_value(data.PZ);
				display_resist_value(data.O2);
				display_resist_value(data.C4);
				display_resist_value(data.FZ);
				printf("\n");
			}
			resistanceUserData.idx = 0;
			resistanceUserData.dataFilled = false;
			--attempt;
		}
	}
	// stop recive resistance
	if (detector_powerDown(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot stop resistance channel. Failed PowerDown mode work");
	}
	free_ResistanceDataListenerHandle(handler);
}
//-----------------------------------------------------------------------------------
int main() {

	printf("Devices scanning...\n");

	P300Scanner* scanner = create_scanner();
	P300InfoArray devInfoArr;
	while (true) {
		if (scanner_get_detectorList(scanner, &devInfoArr) == SDK_NO_ERROR) {
			if (devInfoArr.info_count > 0) {
				break;
			}
		}
		else
		{
			display_sdk_error("Scan failed");
			return 1;
		}
		demo_sleep_ms(50);
	}
	if (devInfoArr.info_count <= 0) {
		printf("Devices not found\n");
		return;
	}
	printf("Devices founded\n\n");

	P300Info info = devInfoArr.info_array[0];
	P300Detector* device = scanner_createDetector(scanner, info.Address);

	display_info(device, &info);

	free_P300InfoArray(devInfoArr);
	scanner_delete(scanner);

	display_battery_power(device);
	display_state(device);

	//display_signal(device);
	display_resist(device);

	detector_delete(device);
	return 0;
}