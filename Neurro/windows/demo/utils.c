#include "utils.h"
#include "stdio.h"


const char* device_status_to_string(const DeviceStatus devStatus) {
	switch (devStatus)
	{
	case Invalid:
		return "Invalid";
	case PowerDown:
		return "PowerDown";
	case Idle:
		return "Idle";
	case Signal:
		return "Signal";
	case Resist:
		return "Resist";
	}
	return "";
}

const char* device_error_to_string(const DeviceError devError) {
	switch (devError)
	{
	case NoError:
		return "NoError";
	case Len:
		return "Len";
	case NoCommand:
		return "NoCommand";
	}
	return "";
}

#ifdef WIN32
#include <windows.h>
#include <synchapi.h>
void demo_sleep_ms(uint32_t ms) {
	Sleep(ms);
}
#else
#include <unistd.h>
void demo_sleep_ms(uint32_t ms) {
	usleep(ms);
}
#endif // WIN32
//MutexSimple create_mutex() {
//    return CreateMutex(
//        NULL,              // default security attributes
//        FALSE,             // initially not owned
//        NULL);             // unnamed mutex
//}
//
//void free_mutex(MutexSimple* mutex) {
//    CloseHandle(*mutex);
//}
//
//bool lock_mutex(MutexSimple* mutex) {
//    return WaitForSingleObject(
//        *mutex,    // handle to mutex
//        INFINITE) == WAIT_OBJECT_0;  // no time-out interval
//}
//
//bool unlcok_mutex(MutexSimple* mutex) {
//    return ReleaseMutex(*mutex);
//}