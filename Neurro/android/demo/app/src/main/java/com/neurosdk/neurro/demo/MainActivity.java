package com.neurosdk.neurro.demo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.neuromd.neurrosdk.ConnectionState;
import com.neuromd.neurrosdk.DeviceState;
import com.neuromd.neurrosdk.DeviceStatus;
import com.neuromd.neurrosdk.NeurroDetector;
import com.neuromd.neurrosdk.NeurroEnumerator;
import com.neuromd.neurrosdk.NeurroInfo;
import com.neuromd.neurrosdk.ResistanceData;
import com.neuromd.neurrosdk.SignalData;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class MainActivity extends AppCompatActivity {
    private Button btnConnect;
    private Button btnResist;
    private Button btnSignal;
    private Button btnPing;
    private Button btnGoIdle;
    private Button btnPowerDown;

    private TextView txtDevConnectionState;
    private TextView txtDevBatteryPower;
    private TextView txtDevState;
    private TextView txtDevInfo;
    private TextView txtRawData;

    private SensorHelper sensorHelper;
    private NeurroDetector device;
    private final AtomicBoolean thConnection = new AtomicBoolean();
    private final AtomicBoolean thRawDataUpd = new AtomicBoolean();

    private final AtomicReference<SignalData> signalDataLast = new AtomicReference<>();
    private final AtomicReference<ResistanceData> resistanceDataLast = new AtomicReference<>();
    private final AtomicReference<DeviceStatus> deviceStatusLast = new AtomicReference<>(DeviceStatus.PowerDown);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorHelper = new SensorHelper(this);

        btnConnect = findViewById(R.id.btn_connect);
        btnResist = findViewById(R.id.btn_resist);
        btnSignal = findViewById(R.id.btn_signal);
        btnPing = findViewById(R.id.btn_ping);
        btnGoIdle = findViewById(R.id.btn_go_idle);
        btnPowerDown = findViewById(R.id.btn_power_down);

        txtDevConnectionState = findViewById(R.id.txt_dev_connection_state);
        txtDevBatteryPower = findViewById(R.id.txt_dev_battery_power);
        txtDevState = findViewById(R.id.txt_dev_state);
        txtDevInfo = findViewById(R.id.txt_dev_info);
        txtRawData = findViewById(R.id.txt_raw_data);

        setButtonState(false);

        btnConnect.setOnClickListener(v -> {
            connectionStateChanged(false);
            btnConnect.setEnabled(false);
            sensorHelper.enabledSensor(new SensorHelper.ISensorEvent() {
                @Override
                public void ready() {
                    connectToDevice();
                }

                @Override
                public void cancel(String message, Exception error) {
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                    btnConnect.setEnabled(true);
                }
            });
        });

        btnResist.setOnClickListener(v -> device.resist());

        btnSignal.setOnClickListener(v -> device.signal());

        btnPing.setOnClickListener(v -> device.ping((byte) 108));

        btnGoIdle.setOnClickListener(v -> device.goIdle());

        btnPowerDown.setOnClickListener(v -> device.powerDown());
    }

    @Override
    protected void onDestroy() {
        thConnection.set(false);
        thRawDataUpd.set(false);
        super.onDestroy();
    }

    private void setButtonState(boolean enabled) {
        btnResist.setEnabled(enabled);
        btnSignal.setEnabled(enabled);
        btnPing.setEnabled(enabled);
        btnGoIdle.setEnabled(enabled);
        btnPowerDown.setEnabled(enabled);
    }

    private void freeDevice() {
        NeurroDetector dev = device;
        device = null;
        if (dev != null) {
            try {
                dev.close();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void connectToDevice() {
        if (!thConnection.getAndSet(true)) {
            Thread th = new Thread(() -> {
                NeurroEnumerator enumerator = new NeurroEnumerator(getApplicationContext(), new NeurroEnumerator.EnumeratorCallback() {
                    @Override
                    public void onDeviceListChanged() {

                    }
                });
                List<NeurroInfo> devices;
                do {
                    devices = enumerator.devices();
                } while (devices.isEmpty() && !Thread.currentThread().isInterrupted() && thConnection.get());
                if (!thConnection.get() || devices.isEmpty())
                    return;

                try {
                    final NeurroInfo devInfo = devices.get(0);
                    while (device == null || device.getConnectionState() != ConnectionState.InRange) {
                        freeDevice();
                        device = enumerator.createDetector(devInfo.address(), new NeurroDetector.DeviceCallback() {
                            @Override
                            public void onBatteryChargeChanged(final int val) {
                                runOnUiThread(() -> updateBattery(val));
                            }

                            @Override
                            public void onConnectionStateChanged(final ConnectionState connectionState) {
                                runOnUiThread(() -> connectionStateChanged(connectionState == ConnectionState.InRange));
                            }

                            @Override
                            public void onDeviceStateChanged(final DeviceState deviceState) {
                                deviceStatusLast.set(deviceState.getStatus());
                                runOnUiThread(() -> updateDevState());
                            }

                            @Override
                            public void onSignalReceived(SignalData signalData) {
                                signalDataLast.set(signalData);
                            }

                            @Override
                            public void onResistanceReceived(ResistanceData resistanceData) {
                                resistanceDataLast.set(resistanceData);
                            }
                        });
                    }
                    enumerator.close();

                    runOnUiThread(() -> {
                        boolean state = device.getConnectionState() == ConnectionState.InRange;
                        setButtonState(state);
                        connectionStateChanged(state);
                        if (state) {
                            updateBattery(device.getBatteryCharge());

                            final StringBuilder sb = new StringBuilder();
                            sb.append("Name: [").append(devInfo.name()).append("] SN: [").append(devInfo.serialNumber()).append("]\n");
                            sb.append("Identifier: [").append(device.getIdentifier()).append("] Version: [").append(device.getFirmwareVersion()).append("]");
                            txtDevInfo.setText(sb.toString());

                            deviceStatusLast.set(device.getDeviceState().getStatus());
                            updateDevState();

                            startUpdateRawData();
                        }
                    });
//                } catch (InterruptedException ignored) {
                } catch (Exception e) {
                    runOnUiThread(() -> {
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        btnConnect.setEnabled(true);
                    });
                } finally {
                    thConnection.set(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }

    private void connectionStateChanged(final boolean state) {
        btnConnect.setEnabled(!state);
        setButtonState(state);
        if (state) {
            txtDevConnectionState.setText(R.string.dev_state_connected);
        } else {
            txtDevConnectionState.setText(R.string.dev_state_disconnected);
            txtDevBatteryPower.setText(R.string.dev_power_empty);
            txtDevState.setText(R.string.dev_power_empty);
            txtDevInfo.setText(null);

            if (device != null) {
                Thread th = new Thread(() -> {
                    try {
                        freeDevice();
                    } catch (Exception ignored) {
                    }
                });
                th.setDaemon(true);
                th.start();
            }
        }
    }

    private void updateBattery(final int val) {
        txtDevBatteryPower.setText(getString(R.string.dev_power_prc, val));
    }

    private void updateDevState() {
        DeviceStatus status = deviceStatusLast.get();
        if (status != null)
            txtDevState.setText(String.valueOf(status));
        else
            txtDevState.setText(null);
    }

    @SuppressWarnings("BusyWait")
    private void startUpdateRawData() {
        if (!thRawDataUpd.getAndSet(true)) {
            Thread th = new Thread(() -> {
                try {
                    while (thRawDataUpd.get() && !Thread.currentThread().isInterrupted()) {
                        if (device != null) {
                            final StringBuilder sb = new StringBuilder();
                            DeviceStatus status = deviceStatusLast.get();
                            if (status == DeviceStatus.Resist) {
                                ResistanceData data = resistanceDataLast.get();
                                if (data != null) {
                                    if (data.getChannel1() >= Double.POSITIVE_INFINITY)
                                        sb.append("Channel1: [∞]\n");
                                    else
                                        sb.append("Channel1: [").append(Math.round(data.getChannel1() / 1000.0)).append(" kΩ]\n");
                                    if (data.getChannel2() >= Double.POSITIVE_INFINITY)
                                        sb.append("Channel2: [∞]\n");
                                    else
                                        sb.append("Channel2: [").append(Math.round(data.getChannel2() / 1000.0)).append(" kΩ]\n");
                                    if (data.getChannel3() >= Double.POSITIVE_INFINITY)
                                        sb.append("Channel3: [∞]\n");
                                    else
                                        sb.append("Channel3: [").append(Math.round(data.getChannel3() / 1000.0)).append(" kΩ]\n");
                                }
                            } else if (status == DeviceStatus.Signal) {
                                SignalData data = signalDataLast.get();
                                if (data != null) {
                                    sb.append("Num: [").append(data.getSampleNumber()).append("]\n");
                                    sb.append("Marker: [").append(data.getMarker()).append("]\n");
                                    sb.append("Channel1: [").append(data.getChannel1()).append("]\n");
                                    sb.append("Channel2: [").append(data.getChannel2()).append("]\n");
                                    sb.append("Channel3: [").append(data.getChannel3()).append("]\n");
                                }
                            }
                            runOnUiThread(() -> txtRawData.setText(sb.toString()));
                        }
                        Thread.sleep(250);
                    }
                } catch (Exception ignored) {
                } finally {
                    thRawDataUpd.set(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }
}