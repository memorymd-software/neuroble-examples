#ifndef NEURRO_DEVICE_H
#define NEURRO_DEVICE_H


#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __ANDROID__
#include <jni.h>
#endif

#include "lib_export.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

	typedef struct _NeurroDetector NeurroDetector;

	typedef struct _NeurroScanner NeurroScanner;

	typedef struct _SignalData
	{
		unsigned short SampleNumber;
		unsigned char Marker;
		double Channel1;
		double Channel2;
		double Channel3;
	} SignalData;

	typedef struct _ResistanceData
	{
		double Channel1;
		double Channel2;
		double Channel3;
	} ResistanceData;

	typedef enum _ConnectionState
	{
		InRange,
		OutOfRange
	} ConnectionState;

	typedef enum _DeviceStatus
	{
		Invalid,
		PowerDown,
		Idle,
		Signal,
		Resist
	} DeviceStatus;

	typedef enum _DeviceError
	{
		NoError,
		Len,
		NoCommand
	} DeviceError;

	typedef struct _NeurroInfo
	{
		char Name[256];
		char Address[256];
		uint64_t SerialNumber;
	} NeurroInfo;

	typedef struct _NeurroInfoArray
	{
		NeurroInfo* info_array;
		size_t info_count;
	} NeurroInfoArray;

	SDK_SHARED void free_NeurroInfoArray(NeurroInfoArray);

	typedef void* DetectorListListenerHandle;
	typedef void* BatteryChargeListenerHandle;
	typedef void* DeviceStatusListenerHandle;
	typedef void* ConnectionStateListenerHandle;
	typedef void* SignalDataListenerHandle;
	typedef void* ResistanceDataListenerHandle;

	SDK_SHARED void free_DetectorListListenerHandle(DetectorListListenerHandle);
	SDK_SHARED void free_BatteryChargeListenerHandle(BatteryChargeListenerHandle);
	SDK_SHARED void free_DeviceStatusListenerHandle(DeviceStatusListenerHandle);
	SDK_SHARED void free_ConnectionStateListenerHandle(ConnectionStateListenerHandle);
	SDK_SHARED void free_SignalDataListenerHandle(SignalDataListenerHandle);
	SDK_SHARED void free_ResistanceDataListenerHandle(ResistanceDataListenerHandle);

#ifdef __ANDROID__
	SDK_SHARED NeurroScanner* create_scanner(JNIEnv* env, jobject context);
	SDK_SHARED JNIEnv* get_jni_env();
	SDK_SHARED jobject get_app_context(JNIEnv* env);
#else
	SDK_SHARED NeurroScanner* create_scanner();
#endif
	SDK_SHARED void scanner_delete(NeurroScanner* scanner);
	SDK_SHARED NeurroDetector* scanner_createDetector(NeurroScanner* scanner, char* identifier_string);
	SDK_SHARED int scanner_get_detectorList(NeurroScanner* scanner, NeurroInfoArray* out_info_array);
	SDK_SHARED int scanner_subscribe_detectorListChanged(NeurroScanner* scanner, void(*)(NeurroScanner*, void* user_data), DetectorListListenerHandle*, void* user_data);

	SDK_SHARED void detector_delete(NeurroDetector* detector);
	SDK_SHARED int detector_get_firmwareVersion(NeurroDetector* device, int* version);
	SDK_SHARED int detector_get_identifier(NeurroDetector* device, char* identifier_string, size_t string_length);
	SDK_SHARED int detector_get_state(NeurroDetector* device, DeviceStatus* out_status, DeviceError* out_error);
	SDK_SHARED int detector_get_connectionState(NeurroDetector* device, ConnectionState* out_state);
	SDK_SHARED int detector_get_batteryCharge(NeurroDetector* device, unsigned char* out_charge);
	SDK_SHARED int detector_subscribe_batteryChargeChanged(NeurroDetector* device, void(*)(NeurroDetector*, unsigned char, void*), BatteryChargeListenerHandle*, void* user_data);
	SDK_SHARED int detector_subscribe_stateChanged(NeurroDetector* device, void(*)(NeurroDetector*, DeviceStatus, DeviceError, void*), DeviceStatusListenerHandle*, void* user_data);
	SDK_SHARED int detector_subscribe_connectionStateChanged(NeurroDetector* device, void(*)(NeurroDetector*, ConnectionState, void*), ConnectionStateListenerHandle*, void* user_data);
	SDK_SHARED int detector_subscribe_signalChannel(NeurroDetector* device, void(*)(NeurroDetector*, SignalData, void*), SignalDataListenerHandle*, void* user_data);
	SDK_SHARED int detector_subscribe_resistanceChannel(NeurroDetector* device, void(*)(NeurroDetector*, ResistanceData, void*), ResistanceDataListenerHandle*, void* user_data);

	SDK_SHARED int detector_powerDown(NeurroDetector* device);
	SDK_SHARED int detector_goIdle(NeurroDetector* device);
	SDK_SHARED int detector_signal(NeurroDetector* device);
	SDK_SHARED int detector_resist(NeurroDetector* device);
	SDK_SHARED int detector_ping(NeurroDetector* device, unsigned char marker);

#ifdef __cplusplus
}
#endif

#endif // NEURRO_DEVICE_H
