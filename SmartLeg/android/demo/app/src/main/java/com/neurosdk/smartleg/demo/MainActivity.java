package com.neurosdk.smartleg.demo;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.neuromd.smartlegsdk.ConnectionState;
import com.neuromd.smartlegsdk.DeviceState;
import com.neuromd.smartlegsdk.DeviceStatus;
import com.neuromd.smartlegsdk.EnvelopeConfigure;
import com.neuromd.smartlegsdk.EnvelopeData;
import com.neuromd.smartlegsdk.ResistanceData;
import com.neuromd.smartlegsdk.SignalData;
import com.neuromd.smartlegsdk.SmartLegDetector;
import com.neuromd.smartlegsdk.SmartLegEnumerator;
import com.neuromd.smartlegsdk.SmartLegInfo;

import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class MainActivity extends AppCompatActivity {
    BroadcastReceiver _receiverBound;

    private Button btnConnect;
    private Button btnResist;
    private Button btnSignal;
    private Button btnEnvelope;
    private Button btnPing;
    private Button btnGoIdle;
    private Button btnPowerDown;

    private TextView txtDevConnectionState;
    private TextView txtDevBatteryPower;
    private TextView txtDevState;
    private TextView txtDevInfo;
    private TextView txtRawData;

    private SensorHelper sensorHelper;
    private SmartLegDetector device;
    private final AtomicBoolean thConnection = new AtomicBoolean();
    private final AtomicBoolean thRawDataUpd = new AtomicBoolean();
    private final AtomicBoolean isEnvelopeStarted = new AtomicBoolean();

    private final AtomicReference<List<SignalData>> signalDataLast = new AtomicReference<>();
    private final AtomicReference<List<EnvelopeData>> envelopeDataLast = new AtomicReference<>();
    private final AtomicReference<ResistanceData> resistanceDataLast = new AtomicReference<>();
    private final AtomicReference<DeviceStatus> deviceStatusLast = new AtomicReference<>(DeviceStatus.PowerDown);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorHelper = new SensorHelper(this);

        btnConnect = findViewById(R.id.btn_connect);
        btnResist = findViewById(R.id.btn_resist);
        btnSignal = findViewById(R.id.btn_signal);
        btnEnvelope = findViewById(R.id.btn_envelope);
        btnPing = findViewById(R.id.btn_ping);
        btnGoIdle = findViewById(R.id.btn_go_idle);
        btnPowerDown = findViewById(R.id.btn_power_down);

        txtDevConnectionState = findViewById(R.id.txt_dev_connection_state);
        txtDevBatteryPower = findViewById(R.id.txt_dev_battery_power);
        txtDevState = findViewById(R.id.txt_dev_state);
        txtDevInfo = findViewById(R.id.txt_dev_info);
        txtRawData = findViewById(R.id.txt_raw_data);

        setButtonState(false);

        btnConnect.setOnClickListener(v -> {
            connectionStateChanged(false);
            btnConnect.setEnabled(false);

            /*BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bAdapter == null)
                return;
            Set<BluetoothDevice> bounded = bAdapter.getBondedDevices();
            bounded.iterator().next().createBond();*/


            sensorHelper.enabledSensor(new SensorHelper.ISensorEvent() {
                @Override
                public void ready() {
                    connectToDevice();
                }

                @Override
                public void cancel(String message, Exception error) {
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                    btnConnect.setEnabled(true);
                }
            });
        });

        btnResist.setOnClickListener(v -> device.resist());

        btnSignal.setOnClickListener(v -> {
            device.setEnvelopeConfig(new EnvelopeConfigure((short) 0, (short) 100));
            device.signal();
            isEnvelopeStarted.set(false);
        });
        btnEnvelope.setOnClickListener(v -> {
            device.setEnvelopeConfig(new EnvelopeConfigure((short) 500, (short) 100));
            isEnvelopeStarted.set(true);
            device.signal();
        });

        btnPing.setOnClickListener(v -> device.ping((byte) 108));

        btnGoIdle.setOnClickListener(v -> device.goIdle());

        btnPowerDown.setOnClickListener(v -> device.powerDown());
    }

    @Override
    protected void onDestroy() {
        thConnection.set(false);
        thRawDataUpd.set(false);
        super.onDestroy();
    }

    private void setButtonState(boolean enabled) {
        btnResist.setEnabled(enabled);
        btnSignal.setEnabled(enabled);
        btnEnvelope.setEnabled(enabled);
        btnPing.setEnabled(enabled);
        btnGoIdle.setEnabled(enabled);
        btnPowerDown.setEnabled(enabled);
    }

    private void freeDevice() {
        SmartLegDetector dev = device;
        device = null;
        if (dev != null) {
            try {
                dev.close();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }


    private boolean boundTo(String devAddress) {
        BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> devBounded = bAdapter.getBondedDevices();
        boolean ready = false;
        for (BluetoothDevice it : devBounded) {
            if (it.getAddress().equals(devAddress)) {
                ready = true;
                break;
            }
        }
        if (!ready) {
            BluetoothDevice dev = bAdapter.getRemoteDevice(devAddress);
            if (dev != null) {
/*
// Bad solution
if (dev.createBond()) {
                    try {
                        Thread.sleep(350);
                        while (dev.getBondState() == BluetoothDevice.BOND_BONDING) {
                            //noinspection BusyWait
                            Thread.sleep(5);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ready = dev.getBondState() == BluetoothDevice.BOND_BONDED;
                }*/
                final ConditionVariable cv = new ConditionVariable();
                final AtomicBoolean boundedReady = new AtomicBoolean(false);
                _receiverBound = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (intent != null) {
                            Bundle extras = intent.getExtras();
                            if (extras != null) {
                                int state = extras.getInt(BluetoothDevice.EXTRA_BOND_STATE);
                                if (state == BluetoothDevice.BOND_BONDED) {
                                    boundedReady.set(true);
                                    cv.open();
                                } else if (state == BluetoothDevice.BOND_NONE) {
                                    cv.open();
                                }
                            }
                        }
                    }
                };
                registerReceiver(_receiverBound, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
                if (dev.createBond()) {
                    cv.block();
                    ready = boundedReady.get();
                }
                unregisterReceiver(_receiverBound);
            }
        }
        return ready;
    }

    @SuppressLint("SetTextI18n")
    private void connectToDevice() {
        if (!thConnection.getAndSet(true)) {
            Thread th = new Thread(() -> {
                SmartLegEnumerator enumerator = new SmartLegEnumerator(getApplicationContext(), new SmartLegEnumerator.EnumeratorCallback() {
                    @Override
                    public void onDeviceListChanged() {

                    }
                });
                List<SmartLegInfo> devices;
                do {
                    devices = enumerator.devices();
                } while (devices.isEmpty() && !Thread.currentThread().isInterrupted() && thConnection.get());
                if (!thConnection.get() || devices.isEmpty())
                    return;

                try {
                    final SmartLegInfo devInfo = devices.get(0);
                    while (device == null || device.getConnectionState() != ConnectionState.InRange) {
                        freeDevice();
                        if (!boundTo(devInfo.address())) {
                            Log.w("Device", "Failed bound");
                            runOnUiThread(() -> connectionStateChanged(false));
                            return;
                        }
                        device = enumerator.createDetector(devInfo.address(), new SmartLegDetector.DeviceCallback() {
                            @Override
                            public void onBatteryChargeChanged(final int val) {
                                runOnUiThread(() -> updateBattery(val));
                            }

                            @Override
                            public void onConnectionStateChanged(final ConnectionState connectionState) {
                                runOnUiThread(() -> connectionStateChanged(connectionState == ConnectionState.InRange));
                            }

                            @Override
                            public void onDeviceStateChanged(final DeviceState deviceState) {
                                deviceStatusLast.set(deviceState.getStatus());
                                if (deviceState.getStatus() == DeviceStatus.Signal) {
                                    isEnvelopeStarted.set(device.getEnvelopeConfig().getMovingAvg() > 0);
                                }
                                runOnUiThread(() -> updateDevState());
                            }

                            @Override
                            public void onSignalReceived(List<SignalData> signalData) {
                                signalDataLast.set(signalData);
                            }

                            @Override
                            public void onResistanceReceived(ResistanceData resistanceData) {
                                resistanceDataLast.set(resistanceData);
                            }

                            @Override
                            public void onEnvelopeReceived(List<EnvelopeData> envelopeData) {
                                envelopeDataLast.set(envelopeData);
                            }
                        });
                    }
                    enumerator.close();

                    runOnUiThread(() -> {
                        boolean state = device.getConnectionState() == ConnectionState.InRange;
                        setButtonState(state);
                        connectionStateChanged(state);
                        if (state) {
                            updateBattery(device.getBatteryCharge());

                            final StringBuilder sb = new StringBuilder();
                            sb.append("Name: [").append(devInfo.name()).append("] SN: [").append(devInfo.serialNumber()).append("]\n");
                            sb.append("Identifier: [").append(device.getIdentifier()).append("] Version: [").append(device.getFirmwareVersion()).append("]");
                            txtDevInfo.setText(sb.toString());

                            deviceStatusLast.set(device.getDeviceState().getStatus());
                            updateDevState();

                            startUpdateRawData();
                        }
                    });
//                } catch (InterruptedException ignored) {
                } catch (Exception e) {
                    runOnUiThread(() -> {
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        btnConnect.setEnabled(true);
                    });
                } finally {
                    thConnection.set(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }

    private void connectionStateChanged(final boolean state) {
        btnConnect.setEnabled(!state);
        setButtonState(state);
        if (state) {
            txtDevConnectionState.setText(R.string.dev_state_connected);
        } else {
            txtDevConnectionState.setText(R.string.dev_state_disconnected);
            txtDevBatteryPower.setText(R.string.dev_power_empty);
            txtDevState.setText(R.string.dev_power_empty);
            txtDevInfo.setText(null);

            if (device != null) {
                Thread th = new Thread(() -> {
                    try {
                        freeDevice();
                    } catch (Exception ignored) {
                    }
                });
                th.setDaemon(true);
                th.start();
            }
        }
    }

    private void updateBattery(final int val) {
        txtDevBatteryPower.setText(getString(R.string.dev_power_prc, val));
    }

    private void updateDevState() {
        DeviceStatus status = deviceStatusLast.get();
        if (status != null)
            txtDevState.setText(String.valueOf(status));
        else
            txtDevState.setText(null);
    }

    @SuppressWarnings("BusyWait")
    private void startUpdateRawData() {
        if (!thRawDataUpd.getAndSet(true)) {
            Thread th = new Thread(() -> {
                try {
                    while (thRawDataUpd.get() && !Thread.currentThread().isInterrupted()) {
                        if (device != null) {
                            final StringBuilder sb = new StringBuilder();
                            DeviceStatus status = deviceStatusLast.get();
                            if (status == DeviceStatus.Resist) {
                                ResistanceData data = resistanceDataLast.get();
                                if (data != null) {
                                    if (data.getChannel1() >= Double.POSITIVE_INFINITY)
                                        sb.append("Ch1: [∞]\n");
                                    else
                                        sb.append("Ch1: [").append(Math.round(data.getChannel1() / 1000.0)).append(" kΩ]\n");

                                    if (data.getChannel2() >= Double.POSITIVE_INFINITY)
                                        sb.append("Ch2: [∞]\n");
                                    else
                                        sb.append("Ch2: [").append(Math.round(data.getChannel2() / 1000.0)).append(" kΩ]\n");

                                    if (data.getChannel3() >= Double.POSITIVE_INFINITY)
                                        sb.append("Ch3: [∞]\n");
                                    else
                                        sb.append("Ch3: [").append(Math.round(data.getChannel3() / 1000.0)).append(" kΩ]\n");

                                    if (data.getChannel4() >= Double.POSITIVE_INFINITY)
                                        sb.append("Ch4: [∞]\n");
                                    else
                                        sb.append("Ch4: [").append(Math.round(data.getChannel4() / 1000.0)).append(" kΩ]\n");

                                    if (data.getChannel5() >= Double.POSITIVE_INFINITY)
                                        sb.append("Ch5: [∞]\n");
                                    else
                                        sb.append("Ch5: [").append(Math.round(data.getChannel5() / 1000.0)).append(" kΩ]\n");

                                    if (data.getChannel6() >= Double.POSITIVE_INFINITY)
                                        sb.append("Ch6: [∞]\n");
                                    else
                                        sb.append("Ch6: [").append(Math.round(data.getChannel6() / 1000.0)).append(" kΩ]\n");
                                }
                            } else if (status == DeviceStatus.Signal && !isEnvelopeStarted.get()) {
                                List<SignalData> data = signalDataLast.get();
                                if (data != null && !data.isEmpty()) {
                                    SignalData it = data.get(data.size() - 1);
                                    sb.append("[Signal]\n");
                                    sb.append("Num: [").append(it.getSampleNumber()).append("]\n");
                                    sb.append("Marker: [").append(it.getMarker()).append("]\n");
                                    sb.append("Ch1: [").append(it.getChannel1() * 1e6).append("]\n");
                                    sb.append("Ch2: [").append(it.getChannel2() * 1e6).append("]\n");
                                    sb.append("Ch3: [").append(it.getChannel3() * 1e6).append("]\n");
                                    sb.append("Ch4: [").append(it.getChannel4() * 1e6).append("]\n");
                                    sb.append("Ch5: [").append(it.getChannel5() * 1e6).append("]\n");
                                    sb.append("Ch6: [").append(it.getChannel6() * 1e6).append("]\n");
                                }
                            } else if (status == DeviceStatus.Signal && isEnvelopeStarted.get()) {
                                List<EnvelopeData> data = envelopeDataLast.get();
                                if (data != null && !data.isEmpty()) {
                                    EnvelopeData it = data.get(data.size() - 1);
                                    sb.append("[Envelope]\n");
                                    sb.append("Num: [").append(it.getPackNumber()).append("]\n");
                                    sb.append("Ch1: [").append(it.getChannel1() * 1e6).append("]\n");
                                    sb.append("Ch2: [").append(it.getChannel2() * 1e6).append("]\n");
                                    sb.append("Ch3: [").append(it.getChannel3() * 1e6).append("]\n");
                                    sb.append("Ch4: [").append(it.getChannel4() * 1e6).append("]\n");
                                    sb.append("Ch5: [").append(it.getChannel5() * 1e6).append("]\n");
                                    sb.append("Ch6: [").append(it.getChannel6() * 1e6).append("]\n");
                                }
                            }

                            runOnUiThread(() -> txtRawData.setText(sb.toString()));
                        }
                        Thread.sleep(250);
                    }
                } catch (Exception ignored) {
                } finally {
                    thRawDataUpd.set(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }
}