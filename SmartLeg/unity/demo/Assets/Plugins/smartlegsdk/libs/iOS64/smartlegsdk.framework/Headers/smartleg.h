#ifndef SMART_LEG_DEVICE_H
#define SMART_LEG_DEVICE_H


#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __ANDROID__
#include <jni.h>
#endif

#include "lib_export.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

	typedef struct _SmartLegDetector SmartLegDetector;

	typedef struct _SmartLegScanner SmartLegScanner;

	typedef struct _SignalData
	{
		unsigned short SampleNumber;
		unsigned char Marker;
		double Channel1;
		double Channel2;
		double Channel3;
		double Channel4;
		double Channel5;
		double Channel6;
	} SignalData;

	typedef struct _ResistanceData
	{
		double Channel1;
		double Channel2;
		double Channel3;
		double Channel4;
		double Channel5;
		double Channel6;
	} ResistanceData;

	typedef struct _EnvelopeData
	{
		unsigned short PackNumber;
		double Channel1;
		double Channel2;
		double Channel3;
		double Channel4;
		double Channel5;
		double Channel6;
	} EnvelopeData;

	typedef struct _EnvelopeConfigure
	{
		/// <summary>
		/// If MovingAvg > 0, then signal envelope enabled.
		/// </summary>
		uint16_t MovingAvg;
		uint16_t Decimation;
	} EnvelopeConfigure;

	typedef enum _ConnectionState
	{
		InRange,
		OutOfRange
	} ConnectionState;

	typedef enum _DeviceStatus
	{
		Invalid,
		PowerDown,
		Idle,
		Signal,
		Resist,
		Envelope
	} DeviceStatus;

	typedef enum _DeviceError
	{
		NoError,
		Len,
		NoCommand
	} DeviceError;

	typedef struct _SmartLegInfo
	{
		char Name[256];
		char Address[256];
		uint64_t SerialNumber;
	} SmartLegInfo;

	typedef struct _SmartLegInfoArray
	{
		SmartLegInfo* info_array;
		size_t info_count;
	} SmartLegInfoArray;

	SDK_SHARED void free_SmartLegInfoArray(SmartLegInfoArray);

	typedef void* DetectorListListenerHandle;
	typedef void* BatteryChargeListenerHandle;
	typedef void* DeviceStatusListenerHandle;
	typedef void* ConnectionStateListenerHandle;
	typedef void* SignalDataListenerHandle;
	typedef void* ResistanceDataListenerHandle;
	typedef void* EnvelopeDataListenerHandle;

	SDK_SHARED void free_DetectorListListenerHandle(DetectorListListenerHandle);
	SDK_SHARED void free_BatteryChargeListenerHandle(BatteryChargeListenerHandle);
	SDK_SHARED void free_DeviceStatusListenerHandle(DeviceStatusListenerHandle);
	SDK_SHARED void free_ConnectionStateListenerHandle(ConnectionStateListenerHandle);
	SDK_SHARED void free_SignalDataListenerHandle(SignalDataListenerHandle);
	SDK_SHARED void free_ResistanceDataListenerHandle(ResistanceDataListenerHandle);

#ifdef __ANDROID__
	SDK_SHARED SmartLegScanner* create_scanner(JNIEnv* env, jobject context);
	SDK_SHARED JNIEnv* get_jni_env();
	SDK_SHARED jobject get_app_context(JNIEnv* env);
#else
	SDK_SHARED SmartLegScanner* create_scanner();
#endif
	SDK_SHARED void scanner_delete(SmartLegScanner* scanner);
	SDK_SHARED SmartLegDetector* scanner_createDetector(SmartLegScanner* scanner, char* identifier_string);
	SDK_SHARED int scanner_get_detectorList(SmartLegScanner* scanner, SmartLegInfoArray* out_info_array);
	SDK_SHARED int scanner_subscribe_detectorListChanged(SmartLegScanner* scanner, void(*)(SmartLegScanner*, void* user_data), DetectorListListenerHandle*, void* user_data);

	SDK_SHARED void detector_delete(SmartLegDetector* detector);
	SDK_SHARED int detector_get_firmwareVersion(SmartLegDetector* device, int* version);
	SDK_SHARED int detector_get_identifier(SmartLegDetector* device, char* identifier_string, size_t string_length);
	SDK_SHARED int detector_get_state(SmartLegDetector* device, DeviceStatus* out_status, DeviceError* out_error);
	SDK_SHARED int detector_get_connectionState(SmartLegDetector* device, ConnectionState* out_state);
	SDK_SHARED int detector_get_batteryCharge(SmartLegDetector* device, unsigned char* out_charge);
	SDK_SHARED int detector_subscribe_batteryChargeChanged(SmartLegDetector* device, void(*)(SmartLegDetector*, unsigned char, void*), BatteryChargeListenerHandle*, void* user_data);
	SDK_SHARED int detector_subscribe_stateChanged(SmartLegDetector* device, void(*)(SmartLegDetector*, DeviceStatus, DeviceError, void*), DeviceStatusListenerHandle*, void* user_data);
	SDK_SHARED int detector_subscribe_connectionStateChanged(SmartLegDetector* device, void(*)(SmartLegDetector*, ConnectionState, void*), ConnectionStateListenerHandle*, void* user_data);
	/**
	* SignalData* array free after callback invoke
	*/
	SDK_SHARED int detector_subscribe_signalChannel(SmartLegDetector* device, void(*)(SmartLegDetector*, SignalData*, size_t, void*), SignalDataListenerHandle*, void* user_data);
	SDK_SHARED int detector_subscribe_resistanceChannel(SmartLegDetector* device, void(*)(SmartLegDetector*, ResistanceData, void*), ResistanceDataListenerHandle*, void* user_data);

	SDK_SHARED int detector_powerDown(SmartLegDetector* device);
	SDK_SHARED int detector_goIdle(SmartLegDetector* device);
	SDK_SHARED int detector_signal(SmartLegDetector* device);
	SDK_SHARED int detector_resist(SmartLegDetector* device);
	SDK_SHARED int detector_ping(SmartLegDetector* device, unsigned char marker);

	SDK_SHARED int detector_get_envelope_config(SmartLegDetector* device, EnvelopeConfigure* out_config);
	SDK_SHARED int detector_set_envelope_config(SmartLegDetector* device, EnvelopeConfigure config);

	/**
	* EnvelopeData* array free after callback invoke
	*/
	SDK_SHARED int detector_subscribe_envelopeChannel(SmartLegDetector* device, void(*)(SmartLegDetector*, EnvelopeData*, size_t, void*), EnvelopeDataListenerHandle*, void* user_data);
	SDK_SHARED void free_EnvelopeDataListenerHandle(EnvelopeDataListenerHandle);

#ifdef __cplusplus
}
#endif

#endif // SMART_LEG_DEVICE_H
