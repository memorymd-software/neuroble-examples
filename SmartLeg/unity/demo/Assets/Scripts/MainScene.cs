﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Threading.Tasks;
using SmartLegSdk;
using System.Threading;

#if UNITY_ANDROID
using UnityEngine.Android;
#endif
public class MainScene : MonoBehaviour
{
    public Button BtnClose;
    public Button BtnConnect;
    public Button BtnResist;
    public Button BtnSignal;
    public Button BtnEnvelope;
    public Button BtnPing;
    public Button BtnGoIdle;
    public Button BtnPowerDown;
    public Text TxtState;
    public Text TxtCharge;
    public Text TxtStatus;
    public Text TxtData;

    private SmartLegEnumerator _enumerator;
    private DeviceInfo? _deviceInfo = null;
    private SmartLegDetector _device;

    private bool _connecting;

    private ViewState _currentState;
    private ViewState _viewState;


    private void CloseApp()
    {
        Application.Quit();
    }

#if UNITY_ANDROID
    private bool BoundDevice(string deviceId)
    {
        AndroidJavaClass clsBluetothAdapter = new AndroidJavaClass("android.bluetooth.BluetoothAdapter");
        AndroidJavaObject bAdapter = clsBluetothAdapter.CallStatic<AndroidJavaObject>("getDefaultAdapter");
        AndroidJavaObject devBounded = bAdapter.Call<AndroidJavaObject>("getBondedDevices");
        bool ready = false;
        var itr = devBounded.Call<AndroidJavaObject>("iterator");
        while (itr.Call<bool>("hasNext"))
        {
            var it = itr.Call<AndroidJavaObject>("next");
            if (it.Call<string>("getAddress").Equals(deviceId))
            {
                ready = true;
                break;
            }
        }
        if (!ready)
        {
            try
            {
                var dev = bAdapter.Call<AndroidJavaObject>("getRemoteDevice", deviceId);
                if (dev != null)
                {
                    AndroidJavaClass clsBluetoothDevice = new AndroidJavaClass("android.bluetooth.BluetoothDevice");
                    // TODO: Nessesary to use "android.content.BroadcastReceiver"!!!
                    // Bad solution
                    if (dev.Call<bool>("createBond")) {
                        Thread.Sleep(300);
                        while (dev.Call<int>("getBondState") == clsBluetoothDevice.GetStatic<int>("BOND_BONDING")) {
                            Thread.Sleep(10);
                        }
                        ready = dev.Call<int>("getBondState") == clsBluetoothDevice.GetStatic<int>("BOND_BONDED");
                    }
                }
            }
            catch (System.Exception e)
            {
                _currentState.Data = e.ToString();
                // TODO: Log error
            }
        }
        return ready;
    }
#endif

    private void StartConnect()
    {
        _currentState.Data = "Scanning...";
        _currentState.BtnConnectEnabled = false;
        BtnConnect.enabled = false; // force
#if !UNITY_ANDROID
        Task.Run(() =>
        {
#endif
        try
        {
            _connecting = false;
            _enumerator = new SmartLegEnumerator();
            _enumerator.EventDetectorListChanged += EventDetectorListChanged;
        }
        catch (System.Exception e)
        {
            _enumerator = null;
            _currentState.Data = $"Error: [{e}]";
        }
#if !UNITY_ANDROID
        });
#endif
    }

    private void StartResist()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    device.Resist();
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void StartSignal()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    var config = device.EnvelopeConfig;
                    if (config.MovingAvg > 0)
                    {
                        config.MovingAvg = 0;
                        config.Decimation = 10;
                        device.EnvelopeConfig = config;
                    }
                    if (device.DeviceState.Status != DeviceStatus.Signal)
                    {
                        device.Signal();
                    }
                    else
                    {
                        _currentState.BtnSignalEnabled = false;
                        _currentState.BtnEnvelopeEnabled = true;
                        _currentState.BtnPingEnabled = true;
                    }
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }
    private void StartEnvelope()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    var config = device.EnvelopeConfig;
                    if (config.MovingAvg == 0)
                    {
                        config.MovingAvg = 10;
                        config.Decimation = 10;
                        device.EnvelopeConfig = config;
                    }
                    if (device.DeviceState.Status != DeviceStatus.Signal)
                    {
                        device.Signal();
                    }
                    else
                    {
                        _currentState.BtnSignalEnabled = true;
                        _currentState.BtnEnvelopeEnabled = false;
                        _currentState.BtnPingEnabled = false;
                    }
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void SendPingMarker()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    device.Ping(123);
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void GoIdle()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    device.GoIdle();
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void PowerDown()
    {
        var device = _device;
        if (device != null)
        {
            Task.Run(() =>
            {
                try
                {
                    device.PowerDown();
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            });
        }
    }

    private void EventDetectorListChanged(SmartLegEnumerator enumerator)
    {
        if (_deviceInfo != null)
            return;
        try
        {
            var devLst = enumerator.Devices;
            if (devLst.Count > 0)
                _deviceInfo = devLst[0];

        }
        catch (System.Exception e)
        {
            _currentState.Data = $"Error: [{e}]";
        }
    }
    private void CheckScanDevice()
    {
        var devInf = _deviceInfo;
        if (devInf != null && !_connecting && !_currentState.Connected)
        {
            _connecting = true;
            _currentState.Data = "Connecting...";
#if !UNITY_ANDROID
            Task.Run(() =>
            {
#else
            if (!BoundDevice(devInf?.Address ?? ""))
            {
                _connecting = false;
                //_currentState.Data = $"Error: [Bound requred]";
                try
                {
                    _deviceInfo = null;
                    _currentState.Connected = false;
                    _currentState.Status = DeviceStatus.Invalid;
                    ResetCurrentState();
                    ClearEnumerator();
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
                return;
            }
#endif
            try
            {
                ClearDevice();
                var device = _enumerator.CreateDetector(devInf?.Address);

                _currentState.DeviceInfoCommon = $"[Name]: [{devInf?.Name}]\n" +
                                    $"[Serial Number]: [{devInf?.SerialNumber}]\n" +
                                    $"[Address]: [{devInf?.Address}]\n" +
                                    $"[Firmware Version]: [{device.FirmwareVersion}]";

                device.EventBatteryChargeChanged += EventBatteryChargeChanged;
                device.EventDetectorConnectionState += EventDetectorConnectionState;
                device.EventDetectorResistanceChannel += EventDetectorResistanceChannel;
                device.EventDetectorSignalChannel += EventDetectorSignalChannel;
                device.EventDetectorEnvelopeChannel += EventDetectorEnvelopeChannel;
                device.EventDetectorStateChanged += EventDetectorStateChanged;

                _currentState.Charge = device.BatteryCharge;
                _currentState.Status = device.DeviceState.Status;
                _currentState.Connected = device.ConnectionState == ConnectionState.InRange;

                _currentState.BtnResistEnabled = _currentState.Status != DeviceStatus.Resist;

                if (_currentState.Status == DeviceStatus.Signal)
                {
                    _currentState.BtnSignalEnabled = device.EnvelopeConfig.MovingAvg > 0;
                    _currentState.BtnEnvelopeEnabled = !_currentState.BtnSignalEnabled;
                    _currentState.BtnPingEnabled = _currentState.BtnEnvelopeEnabled;
                }
                else
                {
                    _currentState.BtnSignalEnabled = true;
                    _currentState.BtnEnvelopeEnabled = true;
                    _currentState.BtnPingEnabled = false;
                }


                _currentState.BtnGoIdleEnabled = _currentState.Status != DeviceStatus.Idle;
                _currentState.BtnPowerDownEnabled = _currentState.Status != DeviceStatus.PowerDown;
                if (_currentState.Status == DeviceStatus.PowerDown || _currentState.Status == DeviceStatus.Idle)
                    _currentState.Data = _currentState.DeviceInfoCommon;

                _device = device;
            }
            catch (System.Exception e)
            {
                _deviceInfo = null;
                _currentState.Data = $"Error: [{e}]";
                _currentState.Connected = false;
                _currentState.Status = DeviceStatus.Invalid;
                ResetCurrentState();
            }
            finally
            {
                _connecting = false;
                try
                {
                    ClearEnumerator();
                }
                catch (System.Exception e)
                {
                    _currentState.Data = $"Error: [{e}]";
                }
            }
#if !UNITY_ANDROID
            });
#endif
        }
    }

    private void EventBatteryChargeChanged(SmartLegDetector detector, int charge)
    {
        _currentState.Charge = charge;
    }
    private void ResetCurrentState()
    {
        _currentState.BtnConnectEnabled = true;
        _currentState.BtnResistEnabled = false;
        _currentState.BtnSignalEnabled = false;
        _currentState.BtnEnvelopeEnabled = false;
        _currentState.BtnPingEnabled = false;
        _currentState.BtnGoIdleEnabled = false;
        _currentState.BtnPowerDownEnabled = false;
        _currentState.Charge = 0;
        _currentState.DeviceInfoCommon = string.Empty;
    }
    private void EventDetectorConnectionState(SmartLegDetector detector, ConnectionState connectionState)
    {
        bool connected = connectionState == ConnectionState.InRange;
        if (connected != _currentState.Connected)
        {
            _currentState.Connected = connected;
            if (!connected)
            {
                ResetCurrentState();
            }
        }
    }
    private void EventDetectorResistanceChannel(SmartLegDetector detector, ResistanceData resistanceData)
    {
        _currentState.Data = string.Format("[Ch1]:[{0:0.00} kOm]\n[Ch2]:[{1:0.00} kOm]\n[Ch3]:[{2:0.00} kOm]\n[Ch4]:[{3:0.00} kOm]\n[Ch5]:[{4:0.00} kOm]\n[Ch6]:[{5:0.00} kOm]\n",
            double.IsInfinity(resistanceData.Channel1) ? resistanceData.Channel1 : resistanceData.Channel1 / 1000.0,
            double.IsInfinity(resistanceData.Channel2) ? resistanceData.Channel2 : resistanceData.Channel2 / 1000.0,
            double.IsInfinity(resistanceData.Channel3) ? resistanceData.Channel3 : resistanceData.Channel3 / 1000.0,
            double.IsInfinity(resistanceData.Channel4) ? resistanceData.Channel4 : resistanceData.Channel4 / 1000.0,
            double.IsInfinity(resistanceData.Channel5) ? resistanceData.Channel5 : resistanceData.Channel5 / 1000.0,
            double.IsInfinity(resistanceData.Channel6) ? resistanceData.Channel6 : resistanceData.Channel6 / 1000.0
            );
    }
    private void EventDetectorSignalChannel(SmartLegDetector detector, IReadOnlyList<SignalData> signalData)
    {
        if (signalData?.Count > 0 && _currentState.BtnEnvelopeEnabled)
        {
            var it = signalData[signalData.Count - 1];
            _currentState.Data = string.Format("[Pack number]:[{0:0}]\n[Marker][{1:0}]\n[Ch1]:[{2:+00000.000;-00000.000} uV]\n[Ch2]:[{3:+00000.000;-00000.000} uV]\n[Ch3]:[{4:+00000.000;-00000.000} uV]\n[Ch4]:[{5:+00000.000;-00000.000} uV]\n[Ch5]:[{6:+00000.000;-00000.000} uV]\n[Ch6]:[{7:+00000.000;-00000.000} uV]\n",
                it.SampleNumber,
                it.Marker,
                it.Channel1 * 1e6,
                it.Channel2 * 1e6,
                it.Channel3 * 1e6,
                it.Channel4 * 1e6,
                it.Channel5 * 1e6,
                it.Channel6 * 1e6
                );
        }
    }
    private void EventDetectorEnvelopeChannel(SmartLegDetector detector, IReadOnlyList<EnvelopeData> envelopeData)
    {
        if (envelopeData?.Count > 0 && _currentState.BtnSignalEnabled)
        {
            var it = envelopeData[envelopeData.Count - 1];
            _currentState.Data = string.Format("[Pack number]:[{0:0}]\n[Ch1]:[{1:+00000.000;-00000.000} uV]\n[Ch2]:[{2:+00000.000;-00000.000} uV]\n[Ch3]:[{3:+00000.000;-00000.000} uV]\n[Ch4]:[{4:+00000.000;-00000.000} uV]\n[Ch5]:[{5:+00000.000;-00000.000} uV]\n[Ch6]:[{6:+00000.000;-00000.000} uV]\n",
                it.PackNumber,
                it.Channel1 * 1e6,
                it.Channel2 * 1e6,
                it.Channel3 * 1e6,
                it.Channel4 * 1e6,
                it.Channel5 * 1e6,
                it.Channel6 * 1e6
                );
        }
    }
    private void EventDetectorStateChanged(SmartLegDetector detector, DeviceState state)
    {
        if (_currentState.Status != state.Status)
        {
            _currentState.Status = state.Status;
            _currentState.BtnResistEnabled = _currentState.Status != DeviceStatus.Resist;

            if (_currentState.Status == DeviceStatus.Signal)
            {
                _currentState.BtnSignalEnabled = detector.EnvelopeConfig.MovingAvg > 0;
                _currentState.BtnEnvelopeEnabled = !_currentState.BtnSignalEnabled;
                _currentState.BtnPingEnabled = _currentState.BtnEnvelopeEnabled;
            }
            else
            {
                _currentState.BtnSignalEnabled = true;
                _currentState.BtnEnvelopeEnabled = true;
                _currentState.BtnPingEnabled = false;
            }
            _currentState.BtnGoIdleEnabled = _currentState.Status != DeviceStatus.Idle;
            _currentState.BtnPowerDownEnabled = _currentState.Status != DeviceStatus.PowerDown;
            if (_currentState.Status == DeviceStatus.PowerDown || _currentState.Status == DeviceStatus.Idle)
                _currentState.Data = _currentState.DeviceInfoCommon;
        }
    }

    private void InitView()
    {
        if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            BtnClose.gameObject.SetActive(false);
        }
        BtnClose.onClick.AddListener(CloseApp);
        BtnConnect.onClick.AddListener(StartConnect);
        BtnResist.onClick.AddListener(StartResist);
        BtnSignal.onClick.AddListener(StartSignal);
        BtnEnvelope.onClick.AddListener(StartEnvelope);
        BtnPing.onClick.AddListener(SendPingMarker);
        BtnGoIdle.onClick.AddListener(GoIdle);
        BtnPowerDown.onClick.AddListener(PowerDown);

        ResetCurrentState();

        _viewState = _currentState;

        BtnConnect.enabled = true;
        BtnResist.enabled = false;
        BtnSignal.enabled = false;
        BtnEnvelope.enabled = false;
        BtnPing.enabled = false;
        BtnGoIdle.enabled = false;
        BtnPowerDown.enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        InitView();
#if UNITY_ANDROID
        Permission.RequestUserPermission("android.permission.BLUETOOTH");
        Permission.RequestUserPermission("android.permission.BLUETOOTH_ADMIN");
        Permission.RequestUserPermission("android.permission.ACCESS_FINE_LOCATION");
        Permission.RequestUserPermission("android.permission.ACCESS_COARSE_LOCATION");
#endif
    }

    private void UpdateStatePanel()
    {
        if (_currentState.Connected != _viewState.Connected)
        {
            _viewState.Connected = _currentState.Connected;
            TxtState.text = _viewState.Connected ? "Connected" : "Disconnected";
        }
        if (_currentState.Charge != _viewState.Charge)
        {
            _viewState.Charge = _currentState.Charge;
            TxtCharge.text = _viewState.Charge > 0 ? $"{_viewState.Charge}%" : "-";
        }
        if (_currentState.DeviceInfoCommon != _viewState.DeviceInfoCommon)
            _viewState.DeviceInfoCommon = _currentState.DeviceInfoCommon;
        if (_currentState.Status != _viewState.Status)
        {
            _viewState.Status = _currentState.Status;
            TxtStatus.text = _viewState.Status != DeviceStatus.Invalid ? _viewState.Status.ToString() : "-";
        }
    }

    private void UpdateViewState()
    {
        UpdateStatePanel();

        if (_currentState.BtnConnectEnabled != _viewState.BtnConnectEnabled)
        {
            _viewState.BtnConnectEnabled = _currentState.BtnConnectEnabled;
            BtnConnect.enabled = _viewState.BtnConnectEnabled;
        }
        if (_currentState.BtnResistEnabled != _viewState.BtnResistEnabled)
        {
            _viewState.BtnResistEnabled = _currentState.BtnResistEnabled;
            BtnResist.enabled = _viewState.BtnResistEnabled;
        }
        if (_currentState.BtnSignalEnabled != _viewState.BtnSignalEnabled)
        {
            _viewState.BtnSignalEnabled = _currentState.BtnSignalEnabled;
            BtnSignal.enabled = _viewState.BtnSignalEnabled;
        }
        if (_currentState.BtnEnvelopeEnabled != _viewState.BtnEnvelopeEnabled)
        {
            _viewState.BtnEnvelopeEnabled = _currentState.BtnEnvelopeEnabled;
            BtnEnvelope.enabled = _viewState.BtnEnvelopeEnabled;
        }
        if (_currentState.BtnPingEnabled != _viewState.BtnPingEnabled)
        {
            _viewState.BtnPingEnabled = _currentState.BtnPingEnabled;
            BtnPing.enabled = _viewState.BtnPingEnabled;
        }
        if (_currentState.BtnGoIdleEnabled != _viewState.BtnGoIdleEnabled)
        {
            _viewState.BtnGoIdleEnabled = _currentState.BtnGoIdleEnabled;
            BtnGoIdle.enabled = _viewState.BtnGoIdleEnabled;
        }
        if (_currentState.BtnPowerDownEnabled != _viewState.BtnPowerDownEnabled)
        {
            _viewState.BtnPowerDownEnabled = _currentState.BtnPowerDownEnabled;
            BtnPowerDown.enabled = _viewState.BtnPowerDownEnabled;
        }
        if (_currentState.Data != _viewState.Data)
        {
            _viewState.Data = _currentState.Data;
            TxtData.text = _viewState.Data;
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckScanDevice();
        UpdateViewState();
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            CloseApp();
        }
    }

    private void ClearDevice()
    {
        var device = _device;
        _device = null;
        if (device != null)
        {
            ResetCurrentState();

            device.EventBatteryChargeChanged -= EventBatteryChargeChanged;
            device.EventDetectorConnectionState -= EventDetectorConnectionState;
            device.EventDetectorResistanceChannel -= EventDetectorResistanceChannel;
            device.EventDetectorSignalChannel -= EventDetectorSignalChannel;
            device.EventDetectorStateChanged -= EventDetectorStateChanged;
            device.Dispose();
        }
    }
    private void ClearEnumerator()
    {
        var enumerator = _enumerator;
        _enumerator = null;
        if (enumerator != null)
        {
            enumerator.EventDetectorListChanged -= EventDetectorListChanged;
            enumerator.Dispose();
        }
    }

    private void OnApplicationQuit()
    {
        try
        {
            var device = _device;
            if (device != null && device.ConnectionState == ConnectionState.InRange)
                device.GoIdle();
            ClearDevice();
            ClearEnumerator();
        }
        catch (System.Exception)
        {
        }
        finally
        {
#if !UNITY_EDITOR
            // Kill all native threads
            System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
        }
    }

    /// <summary>
    /// State of visual components.<br>
    /// Not thread-safe
    /// </summary>
    private struct ViewState
    {
        public bool Connected;
        public int Charge;
        public DeviceStatus Status;
        public string Data;

        public bool BtnConnectEnabled;
        public bool BtnResistEnabled;
        public bool BtnSignalEnabled;
        public bool BtnEnvelopeEnabled;
        public bool BtnPingEnabled;
        public bool BtnGoIdleEnabled;
        public bool BtnPowerDownEnabled;

        public string DeviceInfoCommon;
    }
}
