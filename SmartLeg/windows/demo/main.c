#include "smartleg.h"
#include "sdk_error.h"
#include "utils.h"

#include "stdio.h"
#include <float.h>

#define SIGNAL_SAMPLES_COUNT 20
#define RESIST_SAMPLES_COUNT 3

void display_sdk_error(const char* msg) {
	char errorMsg[1024];
	sdk_last_error_msg(errorMsg, 1024);
	printf("%s. Error desc: %s\n", msg, errorMsg);
}

void display_info(SmartLegDetector* device, SmartLegInfo* info) {
	printf("[Device]:[%s]\n[Serial number]:[%llu]\n[Address]:[%s]\n", info->Name, info->SerialNumber, info->Address);

	int version;
	if (detector_get_firmwareVersion(device, &version) == SDK_NO_ERROR) {
		printf("[Version]:[%d]\n", version);
	}
	else {
		display_sdk_error("Cannot get device version");
	}
}

void display_battery_power(SmartLegDetector* device) {
	unsigned char batPower = 0;
	if (detector_get_batteryCharge(device, &batPower) == SDK_NO_ERROR) {
		printf("[Battery power]:[%d%%]\n", batPower);
	}
	else
	{
		display_sdk_error("Cannot get battery power");
	}
}

void display_state(SmartLegDetector* device) {
	DeviceStatus devStatus;
	DeviceError devError;
	if (detector_get_state(device, &devStatus, &devError) == SDK_NO_ERROR) {
		printf("[Device status]:[%s]\n[Device error]:[%s]\n", device_status_to_string(devStatus), device_error_to_string(devError));
	}
	else {
		display_sdk_error("Cannot get state");
	}
}

void display_envelope_configure(SmartLegDetector* device) {
	printf("\n----- Envelope configure -----\n");

	EnvelopeConfigure envelopeConfig;
	if (detector_get_envelope_config(device, &envelopeConfig) == SDK_NO_ERROR) {
		printf("[Moving Average Window Size]:[%d]\n[Decimation]:[%d]\n", envelopeConfig.MovingAvg, envelopeConfig.Decimation);
	}
	else {
		display_sdk_error("Cannot get envelope config");
	}
}

//-----------------------------------------------------------------------------------
//------------------------------ Signal ---------------------------------------------
//-----------------------------------------------------------------------------------
typedef struct _SignalUserData {
	SignalData data[SIGNAL_SAMPLES_COUNT];
	size_t idx;
	// data buffer is full
	bool dataFilled;
} SignalUserData;

void signal_data_process(SmartLegDetector* device, SignalData* dataArray, size_t dataSize, void* user_data) {
	SignalUserData* signalUserData = (SignalUserData*)user_data;
	if (!signalUserData->dataFilled) {
		size_t idx = 0;
		while (!signalUserData->dataFilled && idx < dataSize) {
			signalUserData->data[signalUserData->idx++] = dataArray[idx++];
			if (signalUserData->idx == SIGNAL_SAMPLES_COUNT)
				signalUserData->dataFilled = true;
		}
	}
}

void display_signal(SmartLegDetector* device) {
	printf("\n----------- Signal -----------\n");

	SignalDataListenerHandle handler;
	SignalUserData signalUserData;
	signalUserData.idx = 0;
	signalUserData.dataFilled = false;
	if (detector_subscribe_signalChannel(device, signal_data_process, &handler, &signalUserData) != SDK_NO_ERROR) {
		display_sdk_error("Cannot subscribe signal channel");
		return;
	}
	// start recive signal
	if (detector_signal(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot start signal channel");
		return;
	}

	int attempt = 50;
	while (attempt > 0) {
		demo_sleep_ms(10);
		if (signalUserData.dataFilled) {
			printf("[N]     [M]         [Ch1]           [Ch2]           [Ch3]           [Ch4]           [Ch5]           [Ch6]\n");
			/*printf("[N]\t[M]\t    [Ch1]\t     [Ch2]\t     [Ch3]\t     [Ch4]\t     [Ch5]\t     [Ch6]\n");*/
			for (int i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
				SignalData data = signalUserData.data[i];
				//printf("%u\t%u\t%.2fuV\t%.2fuV\t%.2fuV\t%.2fuV\t%.2fuV\t%.2fuV\n",
				printf("%u\t%u\t%8.2fuV\t%8.2fuV\t%8.2fuV\t%8.2fuV\t%8.2fuV\t%8.2fuV\n",
					data.SampleNumber,
					data.Marker,
					data.Channel1 * 1e6,
					data.Channel2 * 1e6,
					data.Channel3 * 1e6,
					data.Channel4 * 1e6,
					data.Channel5 * 1e6,
					data.Channel6 * 1e6
				);
			}
			signalUserData.idx = 0;
			signalUserData.dataFilled = false;
			--attempt;

			// send marker ping
			if (detector_ping(device, 123) != SDK_NO_ERROR) {
				display_sdk_error("Cannot ping");
				break;
			}
		}
	}
	// stop recive signal
	if (detector_powerDown(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot stop signal channel. Failed PowerDown mode work");
	}
	free_SignalDataListenerHandle(handler);
}
//-----------------------------------------------------------------------------------
//------------------------------ Resist ---------------------------------------------
//-----------------------------------------------------------------------------------
typedef struct _ResistanceUserData {
	ResistanceData data[RESIST_SAMPLES_COUNT];
	size_t idx;
	// data buffer is full
	bool dataFilled;
} ResistanceUserData;

void resist_data_process(SmartLegDetector* device, ResistanceData data, void* user_data) {
	ResistanceUserData* signalUserData = (ResistanceUserData*)user_data;
	if (!signalUserData->dataFilled) {
		signalUserData->data[signalUserData->idx++] = data;
		if (signalUserData->idx == RESIST_SAMPLES_COUNT)
			signalUserData->dataFilled = true;
	}
}

void display_resist_value(double val) {
	if (val >= DBL_MAX)
		printf("[-----] kOm\t");
	else
		printf("[%.2f] kOm\t", val / 1000);
}

void display_resist(SmartLegDetector* device) {
	printf("\n----------- Resistance -----------\n");

	ResistanceDataListenerHandle handler;
	ResistanceUserData resistanceUserData;
	resistanceUserData.idx = 0;
	resistanceUserData.dataFilled = false;
	if (detector_subscribe_resistanceChannel(device, resist_data_process, &handler, &resistanceUserData) != SDK_NO_ERROR) {
		display_sdk_error("Cannot subscribe signal channel");
		return;
	}
	// start recive resistance
	if (detector_resist(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot start resistance channel");
		return;
	}

	int attempt = 20;
	while (attempt > 0) {
		demo_sleep_ms(10);
		if (resistanceUserData.dataFilled) {
			printf("    [Ch1]\t     [Ch2]\t     [Ch3]\t     [Ch4]\t     [Ch5]\t     [Ch6]\n");
			for (int i = 0; i < RESIST_SAMPLES_COUNT; ++i) {
				ResistanceData data = resistanceUserData.data[i];
				display_resist_value(data.Channel1);
				display_resist_value(data.Channel2);
				display_resist_value(data.Channel3);
				display_resist_value(data.Channel4);
				display_resist_value(data.Channel5);
				display_resist_value(data.Channel6);
				printf("\n");
			}
			resistanceUserData.idx = 0;
			resistanceUserData.dataFilled = false;
			--attempt;
		}
	}
	// stop recive resistance
	if (detector_powerDown(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot stop resistance channel. Failed PowerDown mode work");
	}
	free_ResistanceDataListenerHandle(handler);
}

//-----------------------------------------------------------------------------------
//------------------------------ Envelope ---------------------------------------------
//-----------------------------------------------------------------------------------
typedef struct _EnvelopeUserData {
	EnvelopeData data[SIGNAL_SAMPLES_COUNT];
	size_t idx;
	// data buffer is full
	bool dataFilled;
} EnvelopeUserData;

void endelope_data_process(SmartLegDetector* device, EnvelopeData* dataArray, size_t dataSize, void* user_data) {
	EnvelopeUserData* envelopeUserData = (EnvelopeUserData*)user_data;
	size_t idx = 0;
	while (!envelopeUserData->dataFilled && idx < dataSize) {
		envelopeUserData->data[envelopeUserData->idx++] = dataArray[idx++];
		if (envelopeUserData->idx == SIGNAL_SAMPLES_COUNT)
			envelopeUserData->dataFilled = true;
	}
}

void display_envelope(SmartLegDetector* device) {
	printf("\n----------- Envelope -----------\n");

	EnvelopeDataListenerHandle handler;
	EnvelopeUserData signalUserData;
	signalUserData.idx = 0;
	signalUserData.dataFilled = false;
	if (detector_subscribe_envelopeChannel(device, endelope_data_process, &handler, &signalUserData) != SDK_NO_ERROR) {
		display_sdk_error("Cannot subscribe envelope channel");
		return;
	}
	// start recive signal
	if (detector_signal(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot start signal channel");
		return;
	}
	EnvelopeConfigure envelopeConfig;
	envelopeConfig.MovingAvg = 500;
	envelopeConfig.Decimation = 100;
	// start recive envelope
	if (detector_set_envelope_config(device, envelopeConfig) != SDK_NO_ERROR) {
		display_sdk_error("Cannot start envelope channel");
		return;
	}

	int attempt = 10;
	while (attempt > 0) {
		demo_sleep_ms(10);
		if (signalUserData.dataFilled) {
			printf("[N]         [Ch1]           [Ch2]           [Ch3]           [Ch4]           [Ch5]           [Ch6]\n");
			for (int i = 0; i < SIGNAL_SAMPLES_COUNT; ++i) {
				EnvelopeData data = signalUserData.data[i];
				printf("%u\t%8.2fuV\t%8.2fuV\t%8.2fuV\t%8.2fuV\t%8.2fuV\t%8.2fuV\n",
					data.PackNumber,
					data.Channel1 * 1e6,
					data.Channel2 * 1e6,
					data.Channel3 * 1e6,
					data.Channel4 * 1e6,
					data.Channel5 * 1e6,
					data.Channel6 * 1e6
				);
			}
			signalUserData.idx = 0;
			signalUserData.dataFilled = false;
			--attempt;
		}
	}
	// stop recive envelope
	envelopeConfig.MovingAvg = 0;
	if (detector_set_envelope_config(device, envelopeConfig) != SDK_NO_ERROR) {
		display_sdk_error("Cannot stop envelope channel.");
	}
	// stop recive signal
	if (detector_powerDown(device) != SDK_NO_ERROR) {
		display_sdk_error("Cannot stop signal channel. Failed PowerDown mode work");
	}
	free_EnvelopeDataListenerHandle(handler);
}
//-----------------------------------------------------------------------------------
int main() {

	printf("Devices scanning...\n");

	SmartLegScanner* scanner = create_scanner();
	SmartLegInfoArray devInfoArr;
	while (true) {
		if (scanner_get_detectorList(scanner, &devInfoArr) == SDK_NO_ERROR) {
			if (devInfoArr.info_count > 0) {
				break;
			}
		}
		else
		{
			display_sdk_error("Scan failed");
			return 1;
		}
		demo_sleep_ms(50);
	}
	if (devInfoArr.info_count <= 0) {
		printf("Devices not found\n");
		return;
	}
	printf("Devices founded\n\n");
	SmartLegInfo info = devInfoArr.info_array[0];
	SmartLegDetector* device = scanner_createDetector(scanner, info.Address);

	display_info(device, &info);

	free_SmartLegInfoArray(devInfoArr);
	scanner_delete(scanner);

	display_battery_power(device);
	display_state(device);
	display_envelope_configure(device);

	display_resist(device);
	display_signal(device);
	display_envelope(device);

	detector_delete(device);
	return 0;
}